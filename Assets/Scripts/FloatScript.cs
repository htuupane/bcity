﻿using UnityEngine;

public class FloatScript : MonoBehaviour {

    public float BounceSpeed = 1.0f;
    public float BounceDistance = 1.0f;

    private float originalZ;

	// Use this for initialization
	void Start () {
        originalZ = transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
        //transform.Translate(new Vector3(0f, 0f, BounceDistance * Mathf.Sin(BounceSpeed * Time.time)));
        transform.position = new Vector3(transform.position.x, transform.position.y, originalZ + BounceDistance * Mathf.Sin(BounceSpeed * Time.time));
	}
}
