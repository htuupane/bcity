﻿// Collection of helpful enums and classes

public enum Direction
{
    None    = 0,
    Up      = 1,
    Down    = 2,
    Left    = 3,
    Right   = 4,
}

public enum ShotPower
{
    Low     = 0,
    Med     = 1,
    High    = 2,
}

public enum WallType
{
    Brick       = 0,
    Steel       = 1,
    Adamantium  = 2,
}

public enum PlayerState
{
    Normal      = 0,
    Immobile    = 1,
    Invincible  = 2,
    Dead        = 3,
    Armored     = 4,
    Frozen      = 5,
}

public enum GameType
{
    Coop    = 0,
    Versus  = 1,
    Single  = 2,
    LocalCoop = 3,
}

public enum LevelState
{
    Loading     = 0,
    Starting    = 1,
    Playing     = 2,
    FinishedLose= 3,
    Paused      = 4,
    FinishedWin = 5,

}

public enum AIType
{
    Basic   = 0,
    Fast    = 1,
    Power   = 2,
    Armor   = 3,    // HP 4
    Custom  = 4,
}

public enum PowerUp
{
    None            = 0,
    Star            = 1, // Increase shot power
    Bomb            = 2, // Blow up all enemies on screen
    Shield          = 3, // Shield that absorbs one hit
    Invincibility   = 4, // :D:D
    Shovel          = 5, // Repair base walls, turn them indestructible for a moment
    ExtraLife       = 6, // Extra tank
    StopWatch       = 7, // Freeze enemies
}

public class HelperEnums
{

}
