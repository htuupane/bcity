﻿using UnityEngine;

public class PlayerStats : MonoBehaviour {

    public string Name;
    public int Score;
    public int Tanks;
    public int Stars;
    public bool Armored;

    public float CreationTime;

	// Use this for initialization
	void Start () {
        this.CreationTime = Time.time;
        PlayerStats ps = FindObjectOfType<PlayerStats>();
        if (ps != null)
        {
            if (ps.CreationTime < this.CreationTime)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this);
	}
}
