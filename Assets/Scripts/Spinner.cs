﻿using UnityEngine;

public class Spinner : MonoBehaviour {

    public bool SpinClockwise = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (SpinClockwise)
        {
            transform.Rotate(0f, 0f, -35f * Time.deltaTime);
        }
        else
        {
            transform.Rotate(0f, 0f, 35f * Time.deltaTime);
        }
	}
}
