﻿using UnityEngine;
using System.Collections.Generic;

// Container for all the wall objects in the scene
// Does all the RPC calls for wall destruction
public class WallContainer : Photon.MonoBehaviour {

    public GameObject DestructionParticlesBrick;
    public GameObject DestructionParticlesSteel;
    public GameObject AdamantiumSparkParticles;
    public SoundManager Sound;

    private List<WallScript> levelWalls = null;
    private List<WallScript> baseWalls = null;

	// Use this for initialization
	void Start () {
        levelWalls = new List<WallScript>();
        baseWalls = new List<WallScript>();
        foreach (WallScript w in FindObjectsOfType<WallScript>())
        {
            levelWalls.Add(w);
        } 
        Debug.Log("Found " + levelWalls.Count + " wall segments");
        int baseWallLayer = LayerMask.NameToLayer("BaseWall");
        foreach (WallScript w in levelWalls)
        {
            if (w.gameObject.layer == baseWallLayer)
            {
                baseWalls.Add(w);
            }
        }
        Debug.Log("Found " + baseWalls.Count + " basewall segments");

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void RestoreBaseWalls()
    {
        photonView.RPC("NetworkRestoreBaseWalls", PhotonTargets.AllBufferedViaServer);
    }

    [PunRPC]
    public void NetworkRestoreBaseWalls()
    {
        foreach (WallScript w in baseWalls)
        {
            w.gameObject.SetActive(true);
        }
    }
   
    public void DestroyWall(WallScript wall)
    {
        int wallIndex = levelWalls.IndexOf(wall);
        photonView.RPC("NetworkDestroyWall", PhotonTargets.AllBufferedViaServer, wallIndex);
        photonView.RPC("NetworkDestructionParticles", PhotonTargets.All, wallIndex);    // Also plays sound
        
    }

    [PunRPC]
    public void NetworkDestroyWall(int wallIndex)
    {
        levelWalls[wallIndex].gameObject.SetActive(false);
    }

    [PunRPC]
    public void NetworkDestructionParticles(int wallIndex)
    {
        //levelWalls[wallIndex].gameObject.SetActive(false);
        if (levelWalls[wallIndex].gameObject.CompareTag("WallBrick"))
        {
            GameObject particle = (GameObject)Instantiate(DestructionParticlesBrick, levelWalls[wallIndex].transform.position, Quaternion.identity);
            Destroy(particle, 3f);
        }
        else
        {
            GameObject particle = (GameObject)Instantiate(DestructionParticlesSteel, levelWalls[wallIndex].transform.position, Quaternion.identity);
            Destroy(particle, 3f);
        }
        Sound.PlayExplosion();
    }

    public void SpawnAdamantiumSparks(Vector3 position)
    {
        photonView.RPC("SpawnAdamSparksNetwork", PhotonTargets.All, position);
    }

    [PunRPC]
    public void SpawnAdamSparksNetwork(Vector3 position)
    {
        GameObject particle = (GameObject)Instantiate(AdamantiumSparkParticles, position, Quaternion.identity);
        //Sound.PlayExplosion();
        Destroy(particle, 1.5f);
    }

    public void ResetLevel()
    {
        photonView.RPC("NetworkResetLevel", PhotonTargets.AllBufferedViaServer);
    }

    [PunRPC]
    public void NetworkResetLevel()
    {
        foreach (ShellScript shell in FindObjectsOfType<ShellScript>())
        {
            Destroy(shell.gameObject);  // Make sure no old shells fly around
        }
        foreach (WallScript wall in levelWalls)
        {
            wall.gameObject.SetActive(true);
        }
    }

    public void DestroyShell(GameObject shell)
    {
        // Cannot serialize GameObject
        photonView.RPC("NetworkDestroyShell", PhotonTargets.All, shell);
    }

    [PunRPC]
    public void NetworkDestroyShell(GameObject shell)
    {
        //shell.SetActive(false);
        Destroy(shell);
    }
}
