﻿using System.Collections;
using UnityEngine;

// Handle AI
public class AIMovement : Photon.MonoBehaviour {

    // Worth some points?
    // HP?
    // Type of ai? Aggressive, etc
    // Map reset should remove all ai
    public float speed = 20.0f;
    public float ShotCooldownTime = 0.15f;
    public float ShotSpeed = 8f;
    public ShotPower ShellPower = ShotPower.Low;

    public int HitPoints = 1;
    public int WorthPoints = 100;
    public AIType EnemyType;

    public GameObject ShellPrefab;
    public GameObject ShootPoint;
    public AudioSource Audio;

    public GameObject RayPoint1;
    public GameObject RayPoint2;
    public GameObject RayPoint3;
    public GameObject RayPoint4;

    private PlayerState state;
    private int powerups;

    private bool moving = false;
    public bool ShotCooldown = false;
    private GameObject currentShell;

    private LevelManager levelManager;

    private Vector3 targetLocation;

    // Use this for initialization
    void Start () {
        levelManager = FindObjectOfType<LevelManager>();
        //targetLocation = FindObjectOfType<BaseScript>().transform.position;
        state = PlayerState.Normal;
    }

    // Update is called once per frame
    void Update () {
        if (levelManager.State == LevelState.Playing && state != PlayerState.Frozen &&PhotonNetwork.isMasterClient)
        {
            if (!moving)
            {
                // Move!
                //randomMove();
                // 
                targetLocation = GameObject.FindGameObjectWithTag("Base_A").transform.position;
                moveTowardBase();
            }
            if (!ShotCooldown && currentShell == null)
            {
                // Consider shooting
                int baseWallLayer = LayerMask.NameToLayer("BaseWall");
                Vector3 fwd = transform.TransformDirection(Vector3.up);
                RaycastHit hit;
                bool fire = false;
                if (Physics.Raycast(RayPoint2.transform.position, fwd, out hit, ShotSpeed))
                {
                    if (hit.collider.CompareTag("Player") || hit.collider.CompareTag("RemotePlayer") || hit.collider.CompareTag("Base_A") || hit.collider.gameObject.layer == baseWallLayer)
                    {
                        fire = true;
                    }
                }
                if (Physics.Raycast(RayPoint3.transform.position, fwd, out hit, ShotSpeed))
                {
                    if (hit.collider.CompareTag("Player") || hit.collider.CompareTag("RemotePlayer") || hit.collider.CompareTag("Base_A") || hit.collider.gameObject.layer == baseWallLayer)
                    {
                        fire = true;
                    }
                }
                if (Random.Range(0, 100) > 97)
                {
                    fire = true;
                    
                }
                if (fire)
                {
                    object[] prms = new object[2];
                    prms[0] = (object)ShootPoint.transform.position;
                    prms[1] = (object)ShootPoint.transform.rotation;

                    photonView.RPC("FireShell", PhotonTargets.All, prms);
                    StartCoroutine(shotCD(ShotCooldownTime));
                }
            }
        }
	}

    [PunRPC]
    public void FireShell(Vector3 sp, Quaternion rot)
    {
        // With quick movement shells shoot at slightly different time moments
        //GameObject shell = PhotonNetwork.Instantiate("Shell", ShootPoint.transform.position, ShootPoint.transform.rotation, 0);
        //Instantiate(ShellPrefab, ShootPoint.transform.position, ShootPoint.transform.rotation);
        currentShell = (GameObject)Instantiate(ShellPrefab, sp, rot);
        currentShell.tag = "EnemyShell";
        Audio.Play();
    }

    private IEnumerator shotCD(float cd)
    {
        ShotCooldown = true;
        yield return new WaitForSeconds(cd);
        ShotCooldown = false;
    }

    private void randomMove()
    {
        Direction randomDir = (Direction)Random.Range(0,4);
        StartCoroutine(moveStep(randomDir));
    }

    private void moveTowardBase()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.up);
        Vector3 vectorToBase = targetLocation - transform.position;
        vectorToBase.z = 0.0f;
        //Debug.Log("Vector to base: " + vectorToBase);
        // Check if we get closer to base by moving forward
        //Debug.Log("Horiz dot: " + Vector3.Dot(new Vector3(vectorToBase.x, 0, 0), fwd));
        //Debug.Log("Verti dot: " + Vector3.Dot(new Vector3(0, vectorToBase.y, 0), fwd));
        if (!canMoveForward())
        {
            randomMove();
            return;
        }
        if (Vector3.Dot(new Vector3(vectorToBase.x, 0, 0), fwd) == 0)
        {
            // Up or down
            if (fwd.y < 0 && vectorToBase.y < 0)
            {
                // Above base
                //Debug.Log("Above base");
                StartCoroutine(moveStep(Direction.Down));
            }
            else if (fwd.y > 0 && vectorToBase.y > 0)
            {
                // Below base
                //Debug.Log("Below base");
                StartCoroutine(moveStep(Direction.Up));
            }
            else
            {
                // Facing away from base
                randomMove();
            }
        }
        else if (Vector3.Dot(new Vector3(0, vectorToBase.y, 0), fwd) == 0)
        {
            // Left or right
            // Up or down
            if (fwd.x < 0 && vectorToBase.x < 0)
            {
                // Right side
                //Debug.Log("Right of base");
                StartCoroutine(moveStep(Direction.Left));
            }
            else if (fwd.x > 0 && vectorToBase.x > 0)
            {
                // Left side
                //Debug.Log("Left of base");
                StartCoroutine(moveStep(Direction.Right));
            }
            else
            {
                // Facing away from base
                randomMove();
            }
        }
        else
        {
            if (Mathf.Abs(vectorToBase.x) >= Mathf.Abs(vectorToBase.y))
            {
                // Move horizontally
                if (vectorToBase.x < 0)
                {
                    StartCoroutine(moveStep(Direction.Left));
                }
                else
                {
                    StartCoroutine(moveStep(Direction.Right));
                }
            }
            else
            {
                // Move vertically
                if (vectorToBase.y < 0)
                {
                    StartCoroutine(moveStep(Direction.Down));
                }
                else
                {
                    StartCoroutine(moveStep(Direction.Up));
                }
            }
        }
    }

    private bool canMoveForward()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.up);

        if (Physics.Raycast(RayPoint1.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint2.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint3.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint4.transform.position, fwd, 0.2f))
        {
            //print("There is something in front of the object!");
            return false;
        }
        else return true;
    }

    private IEnumerator moveStep(Direction dir)
    {
        // Turn
        turnToFace(dir);

        if (canMoveForward())
        {
            // Move
            moving = true;
            Vector3 target = transform.position + getMovementStep(dir);
            Vector3 startPos = transform.position;
            float dt = 0.0f;
            while (!V3Equal(target, transform.position))
            {
                dt += Time.deltaTime * speed;
                transform.position = Vector3.Lerp(startPos, target, dt);
                yield return new WaitForEndOfFrame();
            }
            transform.position = target;
            //yield return new WaitForEndOfFrame();
        }
        moving = false;
        yield return new WaitForSeconds(0.001F);
    }
    
    private void turnToFace(Direction dir)
    {
        switch(dir)
        {
            case Direction.Up:
            default:
                transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case Direction.Down:
                transform.rotation = Quaternion.Euler(0, 0, 180);
                break;
            case Direction.Left:
                transform.rotation = Quaternion.Euler(0, 0, 90);
                break;
            case Direction.Right:
                transform.rotation = Quaternion.Euler(0, 0, 270);
                break;
        }
    }

    private Vector3 getMovementStep(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return new Vector3(0, 0.25f, 0);
            case Direction.Down:
                return new Vector3(0, -0.25f, 0);
            case Direction.Right:
                return new Vector3(0.25f, 0, 0);
            case Direction.Left:
                return new Vector3(-0.25f, 0, 0);
            default:
                return Vector3.zero;
        }
    }

    public void TakeHit(ShotPower power)
    {
        // Do something with power. Have HP?
        state = PlayerState.Dead;
        gameObject.SetActive(false);
        if (PhotonNetwork.isMasterClient)
        {
            levelManager.PowerUpSpawnChance();
            PhotonNetwork.Destroy(gameObject);  // Just works!
        }
    }

    public void Freeze()
    {
        StartCoroutine(freezeTime(5.0f));
    }

    private IEnumerator freezeTime(float seconds)
    {
        state = PlayerState.Frozen;
        yield return new WaitForSeconds(seconds);
        state = PlayerState.Normal;
    }

    public void SetEnemyStats(AIType type)
    {
        switch(type)
        {
            case AIType.Basic:
            default:
                HitPoints = 1;
                WorthPoints = 100;
                this.speed = 10f;
                ShotSpeed = 6f;
                ShotCooldownTime = 3f;
                ShellPower = ShotPower.Low;
                EnemyType = AIType.Basic;
                break;
            case AIType.Fast:
                HitPoints = 1;
                WorthPoints = 200;
                this.speed = 30f;
                ShotSpeed = 6f;
                ShotCooldownTime = 1.0f;
                ShellPower = ShotPower.Low;
                EnemyType = AIType.Fast;
                break;
            case AIType.Power:
                HitPoints = 1;
                WorthPoints = 300;
                this.speed = 15f;
                ShotSpeed = 10f;
                ShotCooldownTime = 1.5f;
                ShellPower = ShotPower.High;
                EnemyType = AIType.Power;
                break;
            case AIType.Armor:
                HitPoints = 4;
                WorthPoints = 400;
                this.speed = 12f;
                ShotSpeed = 8f;
                ShotCooldownTime = 2.0f;
                ShellPower = ShotPower.Med;
                EnemyType = AIType.Armor;
                break;

        }
    }

    public void SetEnemyStats(int hp, int points, float speed, float shotSpeed, float shotCooldown)
    {
        HitPoints = hp;
        WorthPoints = points;
        this.speed = speed;
        ShotSpeed = shotSpeed;
        ShotCooldownTime = shotCooldown;
        EnemyType = AIType.Custom;
    }

    // Really move this to somewhere else and make it static
    public bool V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.0001;
    }
}
