﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : Photon.MonoBehaviour
{

    public float speed = 20.0f;
    public float ShotCooldownTime = 0.15f;
    public ShotPower ShellPower = ShotPower.Low;
    public int Tanks = 2;
    public GameObject TankModel;
    public GameObject ShellPrefab;
    public GameObject ShootPoint;
    public GameObject SparkParticles;
    public AudioSource Audio;
    //public Collider FrontCollider;
    public GameObject RayPoint1;
    public GameObject RayPoint2;
    public GameObject RayPoint3;
    public GameObject RayPoint4;
    
    // TEMP
    public GameObject AIPlayer;

    private PlayerState state;
    private int powerups;

    private bool moving = false;
    public bool ShotCooldown = false;
    private GameObject currentShell;

    private LevelManager levelManager;

	// Use this for initialization
	void Start () {
        powerups = 0;
        if (photonView.isMine)
        {
            gameObject.tag = "Player";
        }
        else
        {
            gameObject.tag = "RemotePlayer";
        }
        levelManager = FindObjectOfType<LevelManager>();
        state = PlayerState.Normal;
        if (photonView.isMine)
        {
            levelManager.SetPlayerPower(photonView.viewID, powerups);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (levelManager.State == LevelState.Playing)
        {
            if (state == PlayerState.Dead)
            {
                return; // Need keypresses?
            }
            if (!moving && photonView.isMine && state != PlayerState.Immobile)
            {
                if (Input.GetButton("up"))          // +Y
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    StartCoroutine(moveStep(Direction.Up));
                    //transform.Translate(Vector3.up * speed * Time.deltaTime, Space.World);
                }
                else if (Input.GetButton("down"))   // -Y
                {
                    transform.rotation = Quaternion.Euler(0, 0, 180);
                    StartCoroutine(moveStep(Direction.Down));
                    //transform.Translate(Vector3.down * speed * Time.deltaTime, Space.World);
                }
                else if (Input.GetButton("left"))   // -X
                {
                    transform.rotation = Quaternion.Euler(0, 0, 90);
                    StartCoroutine(moveStep(Direction.Left));
                    //transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
                }
                else if (Input.GetButton("right"))  // +X
                {
                    transform.rotation = Quaternion.Euler(0, 0, 270);
                    StartCoroutine(moveStep(Direction.Right));
                    //transform.Translate(Vector3.right * speed * Time.deltaTime, Space.World);
                }
            }
            if (Input.GetButtonDown("fire"))
            {
                // Shoot
                if (!ShotCooldown && currentShell == null && photonView.isMine)
                {
                    //Instantiate(ShellPrefab, ShootPoint.transform.position, ShootPoint.transform.rotation);
                    //Gameobject clone = (Gameobject)PhotonNetwork.Instantiate("Shell", transform.position, transform.rotation);
                    //GameObject shell = PhotonNetwork.Instantiate("Shell", ShootPoint.transform.position, ShootPoint.transform.rotation, 0);
                    object[] prms = new object[3];
                    prms[0] = (object)ShootPoint.transform.position;
                    prms[1] = (object)ShootPoint.transform.rotation;
                    prms[2] = (object)ShellPower;
                    
                    Audio.Play();
                    photonView.RPC("FireShell", PhotonTargets.All, prms);
                    StartCoroutine(shotCD(ShotCooldownTime));
                }
            }
            if (Input.GetKeyDown(KeyCode.Q) && photonView.isMine && PhotonNetwork.isMasterClient)
            {
                //photonView.RPC("SpawnAI", PhotonTargets.All);
                // Some randomness on targets
                PhotonNetwork.InstantiateSceneObject("AIPlayer", levelManager.GetRandomEnemySpawnPoint().transform.position, Quaternion.identity, 0, null);
            }
            if (Input.GetKeyDown(KeyCode.B) && photonView.isMine && PhotonNetwork.isMasterClient)
            {
                photonView.RPC("PowerUpPickup", PhotonTargets.All, PowerUp.Bomb);
            }
        }
    }

    public void PlayerReset()
    {
        Tanks = 2;
        powerups = 0;
        ShellPower = ShotPower.Low;
        state = PlayerState.Normal;
        TankModel.SetActive(true);
    }

    [PunRPC]
    public void FireShell(Vector3 sp, Quaternion rot, ShotPower pwr)
    {
        // With quick movement shells shoot at slightly different time moments
        //GameObject shell = PhotonNetwork.Instantiate("Shell", ShootPoint.transform.position, ShootPoint.transform.rotation, 0);
        currentShell = (GameObject)Instantiate(ShellPrefab, sp, rot);
        //GameObject shell = shellManager.CreateShell();
        //GameObject shell = PhotonNetwork.Instantiate("Shell", new Vector3(0, 0, 0), Quaternion.identity, 0);
        //shell.transform.position = sp;
        //shell.transform.rotation = rot;
        currentShell.GetComponent<ShellScript>().SetShellStats(5,8f,pwr);
        //(GameObject)Instantiate(ShellPrefab, sp, rot);
        //if (!photonView.isMine)
        //{
        //    shell.GetComponent<ShellScript>().ShellCollider.enabled = false;
        //    // Collisions are only done on one computer
        //}
        currentShell.tag = "Shell";
    }

    private bool canMoveForward()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.up);

        if (Physics.Raycast(RayPoint1.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint2.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint3.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint4.transform.position, fwd, 0.2f))
        {
            return false;
        }
        else return true;
    }

    private IEnumerator moveStep(Direction dir)
    {
        //Debug.Log("Moving. Blocked: " + frontBlocked);
        if (canMoveForward())
        {
            // Move
            moving = true;
            Vector3 target = transform.position + getMovementStep(dir);
            Vector3 startPos = transform.position;
            float dt = 0.0f;
            while (!V3Equal(target,transform.position))
            {
                dt += Time.deltaTime * speed;
                transform.position = Vector3.Lerp(startPos, target, dt);
                yield return new WaitForEndOfFrame();
            }
            transform.position = target;
            //yield return new WaitForEndOfFrame();
        }
        moving = false;
        yield return new WaitForSeconds(0.001F);
    }

    private IEnumerator shotCD(float cd)
    {
        ShotCooldown = true;
        yield return new WaitForSeconds(cd);
        ShotCooldown = false;
    }

    private Vector3 getMovementStep(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return new Vector3(0, 0.25f, 0);
            case Direction.Down:
                return new Vector3(0, -0.25f, 0);
            case Direction.Right:
                return new Vector3(0.25f, 0, 0);
            case Direction.Left:
                return new Vector3(-0.25f, 0, 0);
            default:
                return Vector3.zero;
        }
    }
     
    // Is this the best place to do this?   
    [PunRPC]
    public void PowerUpPickup(PowerUp p)
    {
        switch(p)
        {
            case PowerUp.Star:
                powerups++;
                levelManager.SetPlayerPower(photonView.viewID, powerups);
                if (powerups > 2 && powerups < 6)
                {
                    ShellPower = ShotPower.Med; // Penetration?
                }
                else if (powerups >= 6 )
                {
                    ShellPower = ShotPower.High;
                }
                break;
            case PowerUp.Bomb:
                if (PhotonNetwork.isMasterClient)
                {
                    foreach (GameObject g in GameObject.FindGameObjectsWithTag("AIPlayer"))
                    {
                        PhotonNetwork.Destroy(g); // AI state not changed, problem?
                    }
                }
                break;
            case PowerUp.Shield:
                state = PlayerState.Armored;
                Debug.Log("Shield pickup, id: " + photonView.viewID);
                break;
            case PowerUp.Invincibility:
                StartCoroutine(invincibility(3.0f));
                break;
            case PowerUp.Shovel:
                // Repair base walls
                FindObjectOfType<WallContainer>().RestoreBaseWalls();
                break;
            case PowerUp.ExtraLife:
                Tanks++;
                break;
            case PowerUp.StopWatch:
                // Freeze enemies
                if (PhotonNetwork.isMasterClient)
                {
                    foreach (GameObject g in GameObject.FindGameObjectsWithTag("AIPlayer"))
                    {
                        g.GetComponent<AIMovement>().Freeze();
                    }
                }
                break;
            default:
                Debug.Log("Attempting to use unknown powerup");
                break;
        }
    }

    [PunRPC]
    public void ChangePlayerState(PlayerState newState)
    {
        // Use this to communicate player state to other players

    }

    private IEnumerator invincibility(float time)
    {
        PlayerState originalState = state;  // What about immobilized?
        state = PlayerState.Invincible;
        MeshRenderer tankMesh = TankModel.GetComponentInChildren<MeshRenderer>();
        Color origCol = tankMesh.material.color;
        tankMesh.material.color = Color.red;
        yield return new WaitForSeconds(time);
        tankMesh.material.color = origCol;
        state = originalState;
    }


    public void Immobilize()
    {
        photonView.RPC("ImmobilizeNet", PhotonTargets.All);
    }

    public void EnemyHit()
    {
        photonView.RPC("EnemyHitNet", PhotonTargets.All);
    }

    [PunRPC]
    public void EnemyHitNet()
    {
        // Die unless invincible or armored
        if (state == PlayerState.Invincible)
        {
            return;
        }
        else if (state == PlayerState.Armored)
        {
            state = PlayerState.Normal;
        }
        else
        {
            // Die
            Tanks--;
            if (Tanks <= 0)
            {
                // Game over
                Debug.Log("Player dead!");
                state = PlayerState.Dead;
                TankModel.SetActive(false);
                transform.position = new Vector3(100, 100, 10);
            }
            else
            {
                // Respawn
                Debug.Log("Player respawn");
            }
        }
    }

    [PunRPC]
    public void ImmobilizeNet()
    {
        if (state != PlayerState.Dead)
        {
            //Debug.Log("Immobilizing player " + photonView.viewID);
            state = PlayerState.Immobile;
            SparkParticles.SetActive(true);
            StartCoroutine( immobileCD(3.0f) );
        }
    }

    private IEnumerator immobileCD(float cd)
    {
        state = PlayerState.Immobile;
        yield return new WaitForSeconds(cd);
        state = PlayerState.Normal; // Problems with invincibility?
        SparkParticles.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (photonView.isMine)
        {
            // Use punrpc PowerUpPickup
            if (other.CompareTag("PowerUpStar"))
            {
                photonView.RPC("PowerUpPickup", PhotonTargets.All, PowerUp.Star);
            }
            else if(other.CompareTag("PowerUpShield"))
            {
                photonView.RPC("PowerUpPickup", PhotonTargets.All, PowerUp.Shield);
            }
            else if (other.CompareTag("PowerUpInvincibility"))
            {
                photonView.RPC("PowerUpPickup", PhotonTargets.All, PowerUp.Invincibility);
            }
            else if (other.CompareTag("PowerUpExtraLife"))
            {
                photonView.RPC("PowerUpPickup", PhotonTargets.All, PowerUp.ExtraLife);
            }
            else if (other.CompareTag("PowerUpBomb"))
            {
                photonView.RPC("PowerUpPickup", PhotonTargets.All, PowerUp.Bomb);
            }
            else if (other.CompareTag("PowerUpShovel"))
            {
                photonView.RPC("PowerUpPickup", PhotonTargets.All, PowerUp.Shovel);
            }
            else if (other.CompareTag("PowerUpStopWatch"))
            {
                photonView.RPC("PowerUpPickup", PhotonTargets.All, PowerUp.StopWatch);
            }
            //Destroy(other.gameObject);
        }
        else
        {
            //Destroy(other.gameObject);
        }
        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.Destroy(other.gameObject);
        }
    }

    public bool V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.0001;
    }

}
