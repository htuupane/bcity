﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

// Manage the state in one level
// Also manage GUI?
public class LevelManager : Photon.MonoBehaviour {

    // Should these be set here?
    public static int ShellPenetration;
    public static float ShellSpeed;

    public float EnemySpawnSpeed = 5.0f;
    public int EnemiesInLevel = 20;
    private float startTime;

    //public 

    public Text StateText;
    public Text PowerText;
    public Text PowerAltText;
    public GameObject FinishGameScreenLose;
    public GameObject FinishGameScreenWin;

    public GameObject AIPlayer;
    public int EnemyIndex = 0;

    public List<string> PUNames = new List<string>();

    private LevelState lState = LevelState.Playing;
    private GameObject[] enemySpawnPoints;
    private GameObject[] playerSpawnPoints;
    private GameObject[] puSpawnPoints;

    public Dictionary<int, int> playerPowers;

    public LevelState State
    {
        get { return lState; }
        set {}
    }

    public GameObject[] PlayerSpawnPoints
    {
        get { return playerSpawnPoints; }
        set {}
    }


    void Start () {
        // Do level init
        StateText.text = stateToString(State);
        FinishGameScreenLose.SetActive(false);
        FinishGameScreenWin.SetActive(false);
        enemySpawnPoints = GameObject.FindGameObjectsWithTag("EnemySpawnPoint");
        playerSpawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint");
        puSpawnPoints = GameObject.FindGameObjectsWithTag("PowerUpSpawnPoint");

        Debug.Log("Enemy spawn points found: " + enemySpawnPoints.Length);
        Debug.Log("Player spawn points found: " + playerSpawnPoints.Length);
        Debug.Log("PowerUp spawn points found: " + puSpawnPoints.Length);
        if (playerPowers == null)
        {
            playerPowers = new Dictionary<int, int>();
        }
        Time.timeScale = 1.0f;
        //StartCoroutine(UpdatePlayerStats());
        startTime = Time.time;
    }
	
	void Update () {
        // Debug keys
        if (State == LevelState.Playing)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                PowerUpSpawnChance();
            }
            if (Input.GetKeyDown(KeyCode.Y))
            {
                FindObjectOfType<WallContainer>().RestoreBaseWalls();
            }
        }
        if (State == LevelState.Playing || State == LevelState.Paused)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                PauseGame();
            }
        }
        if (State == LevelState.FinishedLose)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                //resetLevel();
                photonView.RPC("resetLevel", PhotonTargets.AllBufferedViaServer);
            }
        }
        if (State == LevelState.FinishedWin)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                // Next level
                //PhotonNetwork.LoadLevel("NetworkGameTest");
                FinishGameScreenWin.SetActive(false);
                //resetLevel();
                photonView.RPC("resetLevel", PhotonTargets.AllBufferedViaServer);
            }
        }
        if (EnemyIndex < EnemiesInLevel)
        {
            if (photonView.isMine && PhotonNetwork.isMasterClient && startTime + EnemySpawnSpeed < Time.time)
            {
                startTime += EnemySpawnSpeed;
                PhotonNetwork.InstantiateSceneObject("AIPlayer", GetRandomEnemySpawnPoint().transform.position, Quaternion.identity, 0, null);
                photonView.RPC("CurrentEnemyIndex", PhotonTargets.AllBufferedViaServer, EnemyIndex+1);
            }
        }
        else
        {
            // Check if all enemies are dead
            //Debug.Log("20 enemies spawned");
            StartCoroutine(CheckForLiveEnemies());
        }

	}

    [PunRPC]
    public void CurrentEnemyIndex(int index)
    {
        EnemyIndex = index;
    }

    public void PowerUpSpawnChance()
    {
        if (PhotonNetwork.isMasterClient && Random.Range(1,10) >= 5)
        {
            PhotonNetwork.InstantiateSceneObject(PUNames[Random.Range(0,PUNames.Count)], GetRandomPUSpawnPoint().transform.position, Quaternion.identity, 0, null);
        }
    }

    public IEnumerator UpdatePlayerStats()
    {
        yield return new WaitForSeconds(1.0f);
        if (State == LevelState.Playing)
        {
            PowerAltText.text = "";
            foreach (NetworkPlayer p in GameObject.FindObjectsOfType<NetworkPlayer>())
            {
                PowerAltText.text += p.InfoString + "\n";
            }
        }
        StartCoroutine(UpdatePlayerStats());
    } 

    public IEnumerator CheckForLiveEnemies()
    {
        while (FindObjectsOfType<AIMovement>().Length != 0)
        {
            yield return new WaitForSeconds(1.0f);
        }
        WinGame();
    }

    public void PauseGame()
    {
        if (State == LevelState.Playing)
        {
            lState = LevelState.Paused;
            StateText.text = stateToString(State);
            Time.timeScale = 0.0f;
        }
        else if (State == LevelState.Paused)
        {
            lState = LevelState.Playing;
            StateText.text = stateToString(State);
            Time.timeScale = 1.0f;
        }
    }

    public void FinishGame()
    {
        lState = LevelState.FinishedLose;
        StateText.text = stateToString(State);
        FinishGameScreenLose.SetActive(true);
        Time.timeScale = 0.0f;
        // Submit high scores, etc
    }

    public void WinGame()
    {
        lState = LevelState.FinishedWin;
        StateText.text = stateToString(State);
        FinishGameScreenWin.SetActive(true);
        Time.timeScale = 0.0f;
    }

    private string stateToString(LevelState state)
    {
        switch(state)
        {
            case LevelState.Loading:
                return "Loading";
            case LevelState.Starting:
                return "Starting";
            case LevelState.Playing:
                return "Playing";
            case LevelState.FinishedLose:
                return "Finished";
            case LevelState.Paused:
                return "Paused";
            case LevelState.FinishedWin:
                return "Victory";
            default:
                return "Unknown";
        }
    }

    public void SetPlayerPower(int id, int power)
    {
        //PowerText.text = "" + id + ": " + power + "\n";
        if (playerPowers == null)
        {
            playerPowers = new Dictionary<int, int>();
        }
        playerPowers[id] = power;
        if (PhotonNetwork.isMasterClient)
        {
            object[] prms = new object[2];
            prms[0] = id;
            prms[1] = power; 
            photonView.RPC("UpdatePlayerPowers", PhotonTargets.AllBuffered, prms);
        }
        //UpdatePlayerPowerText();
    }

    [PunRPC]
    public void UpdatePlayerPowers(object[] prms)
    {
        if (playerPowers == null)
        {
            playerPowers = new Dictionary<int, int>();
        }
        playerPowers[(int)prms[0]] = (int)prms[1];
        UpdatePlayerPowerText();
    }

    public void UpdatePlayerPowerText()
    {
        PowerText.text = "";
        foreach (KeyValuePair<int, int> k in playerPowers)
        {
            PowerText.text += "" + k.Key + ": " + k.Value + "\n";
        }
    }

    [PunRPC]
    private void resetLevel()
    {
        // Make sure this happens in RPC
        FindObjectOfType<WallContainer>().ResetLevel();
        startTime = Time.time;
        EnemyIndex = 0;
        foreach (PlayerMovement plr in FindObjectsOfType<PlayerMovement>())
        {
            plr.PlayerReset();
            plr.gameObject.transform.position = Vector3.zero; // Get random start position?
        }
        foreach (AIMovement ai in FindObjectsOfType<AIMovement>())
        {
            if (PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.Destroy(ai.gameObject);
            }
        }
        // Destroy powerups?
        lState = LevelState.Playing;

        StateText.text = stateToString(State);
        FinishGameScreenLose.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public GameObject GetRandomEnemySpawnPoint()
    {
        return enemySpawnPoints[Random.Range(0, enemySpawnPoints.Length)];
    }

    public GameObject GetRandomPUSpawnPoint()
    {
        return puSpawnPoints[Random.Range(0, puSpawnPoints.Length)];
    }

}
