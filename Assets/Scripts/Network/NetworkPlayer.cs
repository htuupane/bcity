﻿using UnityEngine;

// Props to Quill18
// https://www.youtube.com/watch?v=oZ7TQjWhtEw
public class NetworkPlayer : Photon.MonoBehaviour {

    // Could use this to exchange information?
    public string InfoString = "";

    private Vector3 realPosition;
    private Quaternion realRotation;

	// Use this for initialization
	void Start () {
        //InfoString = "ASDFGH" + Random.Range(0.0f,10.0f);
    }
	
	// Update is called once per frame
	void Update () {
	    if (!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, realPosition, 0.2f);
            transform.rotation = realRotation;  // Could lerp here if needed.
        }
	}

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // Our player. Send position to network
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            // Someone else's player. We receive this position info
            realPosition = (Vector3)stream.ReceiveNext();
            realRotation = (Quaternion)stream.ReceiveNext();
        }
    }
}
