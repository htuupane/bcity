﻿using UnityEngine;

public class WallScript : MonoBehaviour {

    public WallType WType;
    public bool AlterTextures = false;
    public Vector2 TextureTiling;
    public Vector2 TextureOffset;

	// Use this for initialization
	void Start () {
        if (AlterTextures)
        {
            // Creates copies, bad!
            MeshRenderer rend = GetComponent<MeshRenderer>();
            rend.material.mainTextureOffset = TextureOffset;
            rend.material.mainTextureScale = TextureTiling;
        }

    }
	
	// Update is called once per frame
	void Update () {
	
	}


    public void DestroyWall()
    {
        //photonView.RPC("NetworkDestroyWall", PhotonTargets.AllBufferedViaServer);
        WallContainer wc = FindObjectOfType<WallContainer>();
        if (wc == null)
        {
            FindObjectOfType<Single_WallContainer>().DestroyWall(this);
        }
        else
        {
            wc.DestroyWall(this);
        }
    }

    
}
