﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class NetworkManager : Photon.PunBehaviour
{

    private const string RoomName = "BattleCity";
    private RoomInfo[] roomList;
    public List<PhotonPlayer> CurrentPlayersInRoom = new List<PhotonPlayer>();
    private RoomOptions roomOptions;
    private LevelManager currentLevel;

    // Use this for initialization
    void Start()
    {
        PhotonNetwork.logLevel = PhotonLogLevel.ErrorsOnly;
        PhotonNetwork.ConnectUsingSettings("0.1");
        roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 4;
        currentLevel = FindObjectOfType<LevelManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());


        if (PhotonNetwork.room == null)
        {
            // Create room
            if (GUI.Button(new Rect(100, 100, 250, 100), "Start Server"))
            {
                //PhotonNetwork.CreateRoom(RoomName + System.Guid.NewGuid().ToString("N"));
                PhotonNetwork.CreateRoom(RoomName + System.Guid.NewGuid().ToString("N"), roomOptions, null);
            }

            // Join room
            if (roomList != null)
            {
                for (int i = 0; i < roomList.Length; i++)
                {
                    if (GUI.Button(new Rect(100, 250 + (110 * i), 250, 100), "Join" + roomList[i].name + "\n" + roomList[i].playerCount + " / " + roomList[i].maxPlayers))
                    {
                        PhotonNetwork.JoinRoom(roomList[i].name);
                    }
                }
            }
        }
        /*
        foreach (RoomInfo game in PhotonNetwork.GetRoomList())
        {
            GUILayout.Label(game.name + " " + game.playerCount + "/" + game.maxPlayers);
        }
        */
    }

    public override void OnReceivedRoomListUpdate()
    {
        roomList = PhotonNetwork.GetRoomList();
    }

    void ConnectedToMaster()
    {
        //Debug.Log("MasterYhteys");
    }

    public override void OnJoinedLobby()
    {
        // Tänne tulee koodi mikä ajetaan kun tullaan lobbyyn
        //Debug.Log("Lobbyssa");
        PhotonNetwork.automaticallySyncScene = true;
    }

    public override void OnJoinedRoom()
    {
        // Täällä instansioidaan pelaaja, joka tulee huoneeseen
        StartCoroutine(WaitForSpawnPoints());
        Vector3 randomPoint = currentLevel.PlayerSpawnPoints[Random.Range(0, currentLevel.PlayerSpawnPoints.Length)].transform.position;
        GameObject player = PhotonNetwork.Instantiate("Player", randomPoint, Quaternion.identity, 0);
        //GameObject player = PhotonNetwork.Instantiate("Player", new Vector3(0, 0, 0), Quaternion.identity, 0);
    }

    public IEnumerator WaitForSpawnPoints()
    {
        while (currentLevel.PlayerSpawnPoints == null || currentLevel.PlayerSpawnPoints.Length == 0)
        {
            yield return new WaitForSeconds(0.1f);
        }
    }

    public override void OnCreatedRoom()
    {
        // Täällä voidaan luoda huoneeseen oleellisia juttuja
        // Setup vaihe huoneen tilanteelle.
    }
}
