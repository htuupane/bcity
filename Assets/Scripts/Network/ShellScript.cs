﻿using UnityEngine;
using System.Collections;

// Remember who shot shell for multiplayer scoring
public class ShellScript : Photon.MonoBehaviour
{
    public float ShellSpeed = 10.0f;
    public int Penetration = 5;

    public Collider ShellCollider;

    public ShotPower power; // What does high do?

    public Transform ShellModel;


    void Start()
    {
        Destroy(gameObject, 10.0f);
    }

    void Update()
    {
        transform.Translate(transform.forward * ShellSpeed * Time.deltaTime, Space.World);
        ShellModel.Rotate(0f, 0f, 200f * Time.deltaTime);

    }

    public void SetShellStats(int pen, float speed, ShotPower power)
    {
        Penetration = pen;
        ShellSpeed = speed;
        this.power = power;
    }
    /*
    void FixedUpdate()
    {
        //if (photonView.isMine)
        //{
        //    rayCastCheck();
        //}
    }

    private void rayCastCheck()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;
        if (Physics.Raycast(RayPointML.transform.position, fwd, out hit, 0.35f))
        {
            Debug.Log("Checking collision for hit " + hit.transform.position);
            checkCollision(hit);
        }
        if (Physics.Raycast(RayPointMR.transform.position, fwd, out hit, 0.35f))
        {
            Debug.Log("Checking collision for hit " + hit.transform.position);
            checkCollision(hit);
        }
        if (Physics.Raycast(RayPointL.transform.position, fwd, out hit, 0.35f))
        {
            Debug.Log("Checking collision for hit " + hit.transform.position);
            checkCollision(hit);
        }
        if (Physics.Raycast(RayPointR.transform.position, fwd, out hit, 0.35f))
        {
            Debug.Log("Checking collision for hit " + hit.transform.position);
            checkCollision(hit);
        }
    }

    private void checkCollision(RaycastHit hit)
    {
        if (hit.collider.CompareTag("WallBrick"))
        {
            if (Penetration > 0)
            {
                hit.collider.gameObject.GetComponent<WallScript>().DestroyWall();
                Penetration--;
            }
            if (Penetration <= 0)
            {
                photonView.RPC("DestroyShell", PhotonTargets.All);
                //FindObjectOfType<ShellManager>().DisableShell(GetInstanceID());
                //gameObject.SetActive(false);
            }
        }
    }
    
    [PunRPC]
    public void DestroyShell()
    {
        gameObject.SetActive(false);
        //Destroy(this);
    }
    */
    void OnCollisionEnter(Collision collision)
    {
        // "Legacy" wall
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("WallSegment"))
        {
            /*
            // Check if we can destroy wall? Or just do it with tags?
            collision.gameObject.GetComponent<WallScript>().DestroyWall();
            //photonView.RPC("DestroyObject", PhotonTargets.AllBufferedViaServer, collision.gameObject);
            Penetration--;
            if (Penetration <= 0)
            {
                Destroy(gameObject);
            }
            */
        }
        else if (collision.gameObject.CompareTag("LevelLimit"))
        {
            Destroy(gameObject, 0.2f);
        }

        else if (collision.gameObject.CompareTag("WallBrick"))
        {
            if (Penetration > 0)
            {
                if (PhotonNetwork.isMasterClient)
                {
                    collision.gameObject.GetComponent<WallScript>().DestroyWall();
                }
                Penetration--;
            }
            if (Penetration <= 0)
            {
                //photonView.RPC("DestroyBullet", PhotonTargets.All);
                //FindObjectOfType<ShellManager>().DisableShell(GetInstanceID());
                //FindObjectOfType<ShellManager>().DisableShell(gameObject);
                gameObject.SetActive(false);
                Destroy(gameObject);
                //photonView.RPC("DestroyShell", PhotonTargets.All);
            }
        }

        else if (collision.gameObject.CompareTag("WallSteel"))
        {
            if (power >= ShotPower.Med)
            {
                if (Penetration > 0)
                {
                    if (PhotonNetwork.isMasterClient)
                    {
                        collision.gameObject.GetComponent<WallScript>().DestroyWall();
                    }
                    Penetration--;
                }
                if (Penetration <= 0)
                {
                    //FindObjectOfType<ShellManager>().DisableShell(gameObject);
                    gameObject.SetActive(false);
                    Destroy(gameObject);
                }
            }
            else
            {
                //FindObjectOfType<ShellManager>().DisableShell(gameObject);
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }
        else if (collision.gameObject.CompareTag("WallAdamantium"))
        {
            // Some spark particles?
            // Spawns as many as segments are hit
            FindObjectOfType<WallContainer>().SpawnAdamantiumSparks(transform.position);
            //FindObjectOfType<ShellManager>().DisableShell(gameObject);
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("RemotePlayer") || collision.gameObject.CompareTag("Player"))
        {
            // Network player hit
            if (ShellCollider.CompareTag("Shell"))
            {
                // Immobilize player for a moment
                //collision.gameObject.GetComponent<PlayerMovement>().Immobilize();
                //photonView.RPC("ImmobilizeID", PhotonPlayer.Find(collision.gameObject.GetPhotonView().viewID));
                //Debug.Log("Immobilized id " + collision.gameObject.GetPhotonView().viewID);
                if (PhotonNetwork.isMasterClient)
                {
                    collision.gameObject.GetComponent<PlayerMovement>().Immobilize();
                }
            }
            else if (ShellCollider.CompareTag("EnemyShell"))
            {
                if (PhotonNetwork.isMasterClient)
                {
                    // Kill player
                    collision.gameObject.GetComponent<PlayerMovement>().EnemyHit();
                }
            }
                Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("AIPlayer"))
        {
            // AI hit
            if (PhotonNetwork.isMasterClient)
            {
                collision.gameObject.GetComponent<AIMovement>().TakeHit(power);
            }
            //else
            //{
            //    collision.gameObject.SetActive(false);  // What about hp?
            //}
            //FindObjectOfType<ShellManager>().DisableShell(gameObject);
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("Base_A"))
        {
            //Time.timeScale = 0.0f;
            if (PhotonNetwork.isMasterClient)
            {
                FindObjectOfType<LevelManager>().FinishGame();
            }
            //FindObjectOfType<ShellManager>().DisableShell(gameObject);
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        
    }





    //void OnTriggerEnter(Collider c)
    //{
    //    // Moves too fast and hits more walls than should?
    //    // If wall hit, do bigger collider check?
    //    //Debug.Log("Shell hit: " + c.tag);
    //    //if (ShellCollider.enabled == true)
    //    //{
    //    //    ShellSpeed = 0.0f;
    //    //    ShellCollider.enabled = false;
    //    //    HighCollider.gameObject.SetActive(true);
    //    //    return;
    //    //}
    //    ShellCollider.enabled = false;

    //    Destroy(c.gameObject);
    //    Destroy(gameObject);

    //}

    //private void checkCollisions()
    //{
    //    RaycastHit[] hits;
    //    //hits = Physics.RaycastAll(transform.position, transform.forward, 0.5f);

    //    for (int i = 0; i < 4; i++)
    //    {
    //        if (transform.forward == Vector3.up && transform.forward == Vector3.down)
    //        {
    //            Debug.Log("Facing up or down");
    //            hits = Physics.RaycastAll(transform.position + new Vector3(-0.5f + i*0.25f, 0, 0), transform.forward, 0.2f);
    //            //Gizmos.DrawLine(transform.position + new Vector3(-0.5f + i * 0.25f, 0, 0), transform.position + new Vector3(-0.5f + i * 0.25f, 0.2f, 0));
    //            Debug.DrawLine(transform.position + new Vector3(-0.5f + i * 0.25f, 0, 0), transform.position + new Vector3(-0.5f + i * 0.25f, 0.2f, 0));
    //        }
    //        else
    //        {
    //            Debug.Log("Facing left or right");
    //            hits = Physics.RaycastAll(transform.position + new Vector3(0, -0.5f + i * 0.25f, 0), transform.forward, 0.2f);
    //            //Gizmos.DrawLine(transform.position + new Vector3(0, -0.5f + i * 0.25f, 0), transform.position + new Vector3(0.2f, -0.5f + i * 0.25f, 0));
    //            Debug.DrawLine(transform.position + new Vector3(0, -0.5f + i * 0.25f, 0), transform.position + new Vector3(0.2f, -0.5f + i * 0.25f, 0));
    //        }
    //        for (int j = 0; j < hits.Length; j++)
    //        {
    //            RaycastHit hit = hits[j];
    //            Renderer rend = hit.transform.GetComponent<Renderer>();

    //            if (rend)
    //            {
    //                // Change the material of all hit colliders
    //                // to use a transparent shader.
    //                rend.material.shader = Shader.Find("Transparent/Diffuse");
    //                Color tempColor = rend.material.color;
    //                tempColor.a = 0.3F;
    //                rend.material.color = tempColor;
    //            }
    //        }
    //    }

    //for (int i = 0; i < hits.Length; i++)
    //{
    //    RaycastHit hit = hits[i];
    //    Renderer rend = hit.transform.GetComponent<Renderer>();

    //    if (rend)
    //    {
    //        // Change the material of all hit colliders
    //        // to use a transparent shader.
    //        rend.material.shader = Shader.Find("Transparent/Diffuse");
    //        Color tempColor = rend.material.color;
    //        tempColor.a = 0.3F;
    //        rend.material.color = tempColor;
    //    }
    //}
    //}
}