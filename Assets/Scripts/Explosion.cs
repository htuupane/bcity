﻿using UnityEngine;

public class Explosion : MonoBehaviour {

    public Light Flash;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Flash.intensity -= Time.deltaTime * 15f;
	}
}
