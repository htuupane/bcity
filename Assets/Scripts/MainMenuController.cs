﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

    public Button Singleplayer;
    public Button Multiplayer;
    public Button HighScore;
    public Button Quit;

    public Text HighScoreNames;
    public Text HighScoreScores;

    public GameObject HighScoreContainer;

    public GameObject NameQueryContainer;
    public InputField NameInput;
    public Text InputText;
    public Button ToBattle;

    public RedisClient redcli;

    private bool highScores = true;
    private string nameText, scoreText;

    private PlayerStats ps;

    // Use this for initialization
    void Start () {
        highScores = false;
        HighScoreContainer.SetActive(false);
        NameQueryContainer.SetActive(false);
        // Buttons
        Singleplayer.onClick.AddListener(SingleplayerClicked);
        Multiplayer.onClick.AddListener(MultiplayerClicked);
        HighScore.onClick.AddListener(HighScoreClicked);
        Quit.onClick.AddListener(QuitClicked);
        ToBattle.onClick.AddListener(ToBattleClicked);
        if (ps == null)
        {
            ps = FindObjectOfType<PlayerStats>();
        }
        redcli = FindObjectOfType<RedisClient>();
        Time.timeScale = 1.0f;
    }
	
	// Update is called once per frame
	void Update () {
	    if (highScores)
        {
            // Wait for keypress
            if (Input.anyKeyDown)
            {
                highScores = false;
                HighScoreContainer.SetActive(false);
            }
        }
        if (NameQueryContainer.GetActive() && Input.GetKeyDown(KeyCode.Escape))
        {
            NameQueryContainer.SetActive(false);
        }
	}

    void SingleplayerClicked()
    {
        // Query for name
        NameQueryContainer.SetActive(true);
        if (!ps.Name.Equals("Player"))
        {
            NameInput.text = ps.Name;
        }
    }

    void MultiplayerClicked()
    {
        // Load first network level
        SceneManager.LoadScene("NetworkLevel_01");
    }

    void ToBattleClicked()
    {
        // Load first network level
        if (NameInput.text.Length == 0)
        {
            ps.Name = "Player";
        }
        else
        {
            ps.Name = NameInput.text;
        }
        SceneManager.LoadScene("LocalLevel_01");
    }

    void HighScoreClicked()
    {
        highScores = true;
        HighScoreContainer.SetActive(true);
        HighScoreNames.text = "Fetching...";
        HighScoreScores.text = "Fetching...";
        nameText = "";
        scoreText = "";
        //StartCoroutine(fetchHighScores());
        redcli.GetHighScoreNames();
        StartCoroutine(printHighScores());

        //foreach (string s in redcli.GetHighScoreNames())
        //{
        //    HighScoreNames.text = s + "\n" + HighScoreNames.text;
        //    HighScoreScores.text = redcli.GetScore(s) + "\n" + HighScoreScores.text;
        //}
        //HighScoreNames.text = redcli.GetHighScoreNames()[0];
    }
    /*
    private IEnumerator fetchHighScores()
    {
        foreach (string s in redcli.GetHighScoreNames())
        {
            nameText = s + "\n" + nameText;
            scoreText = redcli.GetScore(s) + "\n" + scoreText;
        }
        yield return new WaitForSeconds(0.001F);
    }
    */
    private IEnumerator printHighScores()
    {
        //Debug.Log("printHighScores start");
        yield return new WaitForSeconds(0.05F);
        while (redcli.returnNameArray == null)
        {
            HighScoreNames.text = "Waiting...";
            yield return new WaitForSeconds(0.05F);
        }
        //Debug.Log("printHighScores: returnNameArray not null");
        foreach (string s in redcli.returnNameArray)
        {
            nameText = s + "\n" + nameText;
        }
        HighScoreNames.text = nameText;
        while (redcli.returnScoreArray == null || redcli.returnScoreArray[redcli.returnScoreArray.Length - 1] == null)
        {
            HighScoreScores.text = "Loading...";
            yield return new WaitForSeconds(0.05F);
        }
        foreach (string s in redcli.returnScoreArray)
        {
            scoreText = s + "\n" + scoreText;
            Debug.Log("ScText: " + s);
        }
        HighScoreScores.text = scoreText;
       // Debug.Log("printHighScores end");
    } 

    void QuitClicked()
    {
        Application.Quit();
    }

    // Coroutine for redis communication?
}
