﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


// A* Pathfinder

public class PathFinder : MonoBehaviour
{

    public int searchLimit = 1000;

    public int LevelWidth, LevelHeight;
    public Single_WallContainer WallCont;

    public GameObject Sprite16;

    //public Material red;
    //public Material blue;

    private List<Node> openList;
    private List<Node> closedList;
    private List<Vec2i> dirs;
    private List<Vec2i> debugResult;
    private Vec2i debugFrom, debugTo;
    private List<Node> debugClosedList;

    private Node[,] nodeMap;

    void Start()
    {
        WallCont = FindObjectOfType<Single_WallContainer>();
        openList = new List<Node>();
        closedList = new List<Node>();
        dirs = new List<Vec2i>();
        dirs.Add(new Vec2i(0, 1));   // N
        dirs.Add(new Vec2i(1, 0));   // E
        dirs.Add(new Vec2i(0, -1));   // S
        dirs.Add(new Vec2i(-1, 0));   // W
        
    }


    void Update()
    {

    }
    

    public List<Vec2i> FindPath(int startx, int starty, int goalx, int goaly)  // int floor? // vec2 walking direction
    {
        Vec2i start = new Vec2i();
        start.x = startx;
        start.x = starty;
        Vec2i goal = new Vec2i();
        goal.x = goalx;
        goal.y = goaly;
        openList = new List<Node>();
        closedList = new List<Node>();
        CreateNodeMap();
        //for (int i = 0; i < LevelHeight*4; i++)
        //{
        //    for (int j = 0; j < LevelWidth*4; j++)
        //    {
        //        if (WallCont.ByteMapWalls[j, i] == 1)
        //        {
        //            Instantiate(Sprite16, new Vector3(j-20, i+24, 0), Quaternion.identity);
        //        }
        //    }
        //}
        //GameObject sp1 = (GameObject)Instantiate(Sprite16, new Vector3(0 - 20, 0 + 24, 0), Quaternion.identity);
        //sp1.GetComponent<SpriteRenderer>().color = Color.blue;
        //GameObject sp2 = (GameObject)Instantiate(Sprite16, new Vector3(4 * LevelWidth -1 - 20, 0 + 24, 0), Quaternion.identity);
        //sp2.GetComponent<SpriteRenderer>().color = Color.blue;
        //GameObject sp3 = (GameObject)Instantiate(Sprite16, new Vector3(0 - 20, 4*LevelHeight-1 + 24, 0), Quaternion.identity);
        //sp3.GetComponent<SpriteRenderer>().color = Color.blue;
        //GameObject sp4 = (GameObject)Instantiate(Sprite16, new Vector3(4 * LevelWidth -1 - 20, 4 * LevelHeight -1 + 24, 0), Quaternion.identity);
        //sp4.GetComponent<SpriteRenderer>().color = Color.blue;
        if (WallCont.MapUpdated)
        {
            CreateNodeMap();
            WallCont.MapUpdated = false;
        }
        else
        {
            foreach (Node n in nodeMap)
            {
                n.g = 1000000;
                n.f = 1000000;
            }
        }
        List<Vec2i> result = new List<Vec2i>();
        bool pathFound = false;
        openList.Add(nodeMap[startx, starty]);
        openList[0].g = 0.0f;
        openList[0].h = Mathf.Sqrt(((float)goalx - startx) * (goalx - startx) + (goaly - starty) * (goaly - starty)); // * ???
        //System.Console.WriteLine("{0:5}", openList[0].h);
        openList[0].f = openList[0].h;
        openList[0].parent = openList[0];
        //Debug.Log("" + openList[0].parent.pos);
        int attempts = 0;

        while (openList.Count > 0 && attempts < searchLimit)
        {
            // Do the magic
            openList.Sort();
            attempts++;

            Node currentNode = openList[0];
            //Node currentNode = openList.Min();
            openList.RemoveAt(0);
            closedList.Add(currentNode.Copy());
            
            if (currentNode.pos.x == goalx && currentNode.pos.y == goaly)
            {
                // We found a path!
                pathFound = true;
                result.Add(currentNode.pos);
                Vec2i tempPos = currentNode.parent.pos;
                //closedList.Sort();
                closedList.Reverse();   // Not really needed
                //int count = 0;

                //Debug.Log("1-12 " + nodeMap[1,12].parent.pos);
                //Debug.Log("0-13 " + nodeMap[0, 13].parent.pos);
                
                for (int i = 0; i < closedList.Count; i++)
                {
                    if (tempPos == start)
                    {
                        break;
                    }
                    else if (tempPos == closedList[i].pos)
                    {
                        result.Add(tempPos);
                        tempPos = closedList[i].parent.pos;
                    }
                }
                break; // Break when goal is found
            }
            foreach (Vec2i n in currentNode.neighbors)
            {
                bool skip = false;
                Node neighbor = nodeMap[n.x, n.y];
                //int newCost = costSoFar + GetCost(currentNode.pos, n);
                float tentative_g = currentNode.g + 1; // + TurnCost(facing, n);
                //neighbor.g = currentNode.g + GetCost(currentNode.pos, n); // + TurnCost(facing, n);
                //neighbor.h = Mathf.Sqrt(Mathf.Pow((goal.x - neighbor.pos.x), 2.0f) + Mathf.Pow((goal.y - neighbor.pos.y), 2.0f));   // Count differently?
                //neighbor.h = Mathf.Abs(goal.x - neighbor.pos.x) + Mathf.Abs(goal.y - neighbor.pos.y);
                //neighbor.f = neighbor.g + neighbor.h;
                //neighbor.parent = currentNode;

                foreach (Node closedNode in closedList)
                {
                    if (closedNode.pos == neighbor.pos)
                    {
                        skip = true;
                    }
                }

                foreach (Node openNode in openList)
                {
                    if (openNode.pos == neighbor.pos && openNode.g > tentative_g)
                    {
                        openNode.g = tentative_g;
                        openNode.h = Mathf.Sqrt(Mathf.Pow((goal.x - neighbor.pos.x), 2.0f) + Mathf.Pow((goal.y - neighbor.pos.y), 2.0f));
                        openNode.f = openNode.h + tentative_g;
                        openNode.parent = currentNode;
                        skip = true;
                    }
                    else if (openNode.pos == neighbor.pos)
                    {
                        skip = true;
                    }
                }


                if (!skip)
                {
                    // Tsekkaa voiko 4x4 liikkua
                    // Pitäskö tsekkaa out of bounds?
                    Vec2i nbrDir = new Vec2i(neighbor.pos.x - currentNode.pos.x, neighbor.pos.y - currentNode.pos.y);
                    bool canFit = false;
                    if (Mathf.Sqrt(nbrDir.x * nbrDir.x + nbrDir.y * nbrDir.y) != 1.0f)
                    {
                        Debug.Log("Neighbour not adjacent: " + nbrDir.x + ", " + nbrDir.y);
                    }
                    if (nbrDir.x == -1) // Left
                    {
                        if (WallCont.ByteMapWalls[neighbor.pos.x, neighbor.pos.y + 1] == 0 &&
                            WallCont.ByteMapWalls[neighbor.pos.x, neighbor.pos.y + 2] == 0 &&
                            WallCont.ByteMapWalls[neighbor.pos.x, neighbor.pos.y + 3] == 0)
                        {
                            canFit = true;
                        }
                    }
                    else if (nbrDir.x == 1) // Right
                    {
                        if (neighbor.pos.x + 3 < LevelWidth*4)
                        {
                            if (WallCont.ByteMapWalls[neighbor.pos.x + 3, neighbor.pos.y + 1] == 0 &&
                                WallCont.ByteMapWalls[neighbor.pos.x + 3, neighbor.pos.y + 2] == 0 &&
                                WallCont.ByteMapWalls[neighbor.pos.x + 3, neighbor.pos.y + 3] == 0 &&
                                WallCont.ByteMapWalls[neighbor.pos.x + 3, neighbor.pos.y] == 0)
                            {
                                canFit = true;
                            }
                        }
                    }
                    else if (nbrDir.y == -1) // Down
                    {
                        if (WallCont.ByteMapWalls[neighbor.pos.x+1, neighbor.pos.y] == 0 &&
                            WallCont.ByteMapWalls[neighbor.pos.x+2, neighbor.pos.y] == 0 &&
                            WallCont.ByteMapWalls[neighbor.pos.x+3, neighbor.pos.y] == 0)
                        {
                            canFit = true;
                        }
                    }
                    else if (nbrDir.y == 1) // Up
                    {
                        if (neighbor.pos.y + 3 < LevelHeight*4)
                        {
                            if (WallCont.ByteMapWalls[neighbor.pos.x + 1, neighbor.pos.y + 3] == 0 &&
                                WallCont.ByteMapWalls[neighbor.pos.x + 2, neighbor.pos.y + 3] == 0 &&
                                WallCont.ByteMapWalls[neighbor.pos.x + 3, neighbor.pos.y + 3] == 0 &&
                                WallCont.ByteMapWalls[neighbor.pos.x, neighbor.pos.y + 3] == 0)
                            {
                                canFit = true;
                            }
                        }
                    }
                    if (canFit)
                    {
                        //Debug.Log("Adding to open list: " + neighbor.pos.x + ", " + neighbor.pos.y);
                        neighbor.g = tentative_g;
                        neighbor.h = Mathf.Sqrt(Mathf.Pow((goal.x - neighbor.pos.x), 2.0f) + Mathf.Pow((goal.y - neighbor.pos.y), 2.0f));
                        neighbor.f = neighbor.g + neighbor.h;
                        neighbor.parent = currentNode;
                        openList.Add(neighbor);
                    }
                }
            }

        }
        //Debug.Log("On open list: " + openList.Count);
        //Debug.Log("On closed list: " + closedList.Count);
        if (pathFound)
        {
            //foreach (Vec2i v in result)
            //{
            //    //GameObject go = (GameObject)Instantiate(Sprite16, new Vector3(v.x - 20, v.y + 24, 0), Quaternion.identity);
            //    //go.GetComponent<SpriteRenderer>().color = Color.red;
            //    //go.GetComponent<SpriteRenderer>().sprite.
            //}
            //Debug.Log("Found path with " + closedList.Count + " closed steps!");
            //Debug.Log("Found result path with " + result.Count + " steps! It took " + attempts + " attempts.");
            //string steps = "";
            /*
            foreach (Node n in closedList)
            {
                gameScript.levelFloors[(int)n.pos.x, (int)n.pos.y].GetComponent<Renderer>().material = blue;
            }
            foreach (Vector2 n in result)
            {
                //steps += ("(" + n.x + "," + n.y + ") ");
                gameScript.levelFloors[(int)n.x, (int)n.y].GetComponent<Renderer>().material = red;
                //Debug.Log("Node " + n.pos + " - Parent " + n.parent.pos);
            }
            //Debug.Log(steps);
            */

        }
        else if (attempts == searchLimit)
        {
            Debug.Log("Path end point too far.");
        }
        else
        {
            //UnityEditor.EditorApplication.isPaused = true;
            Debug.Log("No path found.");
            Debug.Log("Closed list size: " + closedList.Count);
            Debug.Log("From: " + startx + ", " + starty + "  to: " + goalx + ", " + goaly);
            debugClosedList = closedList;
            debugFrom = new Vec2i(startx, starty);
            debugTo = new Vec2i(goalx, goaly);
        }
        //return result;
        debugResult = result;
        return result;
    }
    
    public void CreateNodeMap()
    {
        if (nodeMap == null)
        {
            nodeMap = new Node[LevelWidth * 4, LevelHeight * 4];
            //Debug.Log("New nodemap: " + LevelWidth * 4 + " x " + LevelHeight * 4);
            for (int i = 0; i < LevelHeight * 4; i++)
            {
                for (int j = 0; j < LevelWidth * 4; j++)
                {
                    Node tempNode = new Node();
                    tempNode.pos = new Vec2i(j, i);
                    tempNode.g = 1000000;
                    tempNode.f = 1000000;
                    // Create neighbors
                    foreach (Vec2i d in dirs)
                    {
                        if (d.y + i < 0 || d.y + i > LevelHeight * 4 - 1)
                        {
                            continue;
                        }
                        else if (d.x + j < 0 || d.x + j > LevelWidth * 4 - 1)
                        {
                            continue;
                        }
                        else
                        {

                            if (WallCont.ByteMapWalls[j + d.x, i + d.y] == 0)
                            {
                                tempNode.neighbors.Add(new Vec2i(d.x + j, d.y + i));
                            }
                            //tempNode.neighbors.Add(new Vector2(d.x + i, d.y + j));
                        }
                    }
                    nodeMap[j, i] = tempNode;
                }
            }
        }
        else
        {
            for (int i = 0; i < LevelHeight * 4; i++)
            {
                for (int j = 0; j < LevelWidth * 4; j++)
                {
                    nodeMap[j, i].g = 1000000;
                    nodeMap[j, i].f = 1000000;
                    nodeMap[j, i].neighbors = new List<Vec2i>();
                    foreach (Vec2i d in dirs)
                    {
                        if (d.y + i < 0 || d.y + i > LevelHeight * 4 - 1)
                        {
                            continue;
                        }
                        else if (d.x + j < 0 || d.x + j > LevelWidth * 4 - 1)
                        {
                            continue;
                        }
                        else
                        {

                            if (WallCont.ByteMapWalls[j + d.x, i + d.y] == 0)
                            {
                                nodeMap[j, i].neighbors.Add(new Vec2i(d.x + j, d.y + i));
                            }
                            //tempNode.neighbors.Add(new Vector2(d.x + i, d.y + j));
                        }
                    }
                }
            }
        }
    }
    /*
    void OnDrawGizmos()
    {
        if (debugResult != null)
        {
            for (int i = 0; i < debugResult.Count; i++)
            {
                //Debug.DrawLine(new Vector3(result[i].x * 0.25f - 14 * 2, result[i].y * 0.25f - 12 * 2, -2),
                //    new Vector3(result[i + 1].x * 0.25f - 14 * 2, result[i + 1].y * 0.25f - 12 * 2, -2));
                Gizmos.color = Color.magenta;
                Gizmos.DrawSphere(new Vector3(1*(debugResult[i].x * 0.25f - LevelWidth / 2 + 0.125f), 1*(debugResult[i].y * 0.25f - LevelHeight / 2 + 0.125f), -2), 0.1f);
            }
        }
        if (debugFrom !=  debugTo)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(new Vector3(debugFrom.x * 0.25f - LevelWidth / 2 + 0.125f, debugFrom.y * 0.25f - LevelHeight / 2 + 0.125f, -3),
                new Vector3(debugTo.x * 0.25f - LevelWidth / 2 + 0.125f, debugTo.y * 0.25f - LevelHeight / 2 + 0.125f, -2));
        }
        if (debugClosedList != null)
        {
            for (int i = 0; i < debugClosedList.Count; i++)
            {
                //Debug.DrawLine(new Vector3(result[i].x * 0.25f - 14 * 2, result[i].y * 0.25f - 12 * 2, -2),
                //    new Vector3(result[i + 1].x * 0.25f - 14 * 2, result[i + 1].y * 0.25f - 12 * 2, -2));
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(new Vector3(1 * (debugClosedList[i].pos.x * 0.25f - LevelWidth / 2 + 0.125f), 1 * (debugClosedList[i].pos.y * 0.25f - LevelHeight / 2 + 0.125f), -2), 0.12f);
            }
        }
        
    }
    */
    public float TurnCost(Vector2 a, Vector2 b)
    {
        //float result = 0.0f;
        if (a == b)
        {
            return 0.0f;
        }
        else return 0.0f;
        //return result;
    }
}

public struct Vec2i
{
    public int x;
    public int y;

    public Vec2i(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    public override bool Equals(System.Object obj)
    {
        return obj is Vec2i && this == (Vec2i)obj;
    }
    public override int GetHashCode()
    {
        return x.GetHashCode() ^ y.GetHashCode();
    }
    public static bool operator ==(Vec2i a, Vec2i b)
    {
        return a.x == b.x && a.y == b.y;
    }
    public static bool operator !=(Vec2i a, Vec2i b)
    {
        return !(a == b);
    }
}

public class Node : IComparable<Node>
{
    public Node parent;
    public Vec2i pos;
    public float f, g, h;
    public List<Vec2i> neighbors = new List<Vec2i>();

    int IComparable<Node>.CompareTo(Node n)
    {
        if (f > n.f)
        {
            return 1;
        }
        else if (n.f > f)
        {
            return -1;
        }
        else return 0;

    }

    public Node Copy()
    {
        Node copyNode = new Node();
        copyNode.parent = this.parent;
        copyNode.pos = this.pos;
        copyNode.f = this.f;
        copyNode.g = this.g;
        copyNode.h = this.h;
        copyNode.neighbors = this.neighbors;

        return copyNode;
    }

}