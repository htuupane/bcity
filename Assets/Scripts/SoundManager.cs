﻿using UnityEngine;

public class SoundManager : MonoBehaviour {

    public AudioClip ShootSound;
    public AudioClip ExplosionSound;
    public AudioClip PowerUpSound;
    public AudioClip HitSound;
    public AudioClip DeathSound;
    public AudioClip HitSoundEnemy;
    public AudioClip ExplosionBomb;
    public AudioClip InvincibilityPickup;
    public AudioClip Victory;

    public AudioClip MusicLoop;

    public AudioSource SoundSource;
    public AudioSource MusicSource;


    // Use this for initialization
    void Start () {
        MusicSource.Play();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayExplosion()
    {
        SoundSource.Play();
    }
}
