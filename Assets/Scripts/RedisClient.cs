﻿using UnityEngine;
using TeamDev.Redis;
//using System.Diagnostics;
using System.Threading;

public class RedisClient : MonoBehaviour {

    private RedisDataAccessProvider redis;
    public string[] returnNameArray;
    public string[] returnScoreArray;
    public string returnText;

	// Use this for initialization
	void Awake () {
        if (FindObjectsOfType<RedisClient>().Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
        redis = new RedisDataAccessProvider();
        //redis.CheckConnectionStatus();
        //redis.Close();
        //redis.Configuration.Host = "ttredis.duckdns.org";
        ////redis.Configuration.Host = "192.168.1.51";
        //redis.Configuration.Port = 63791;
        //redis.Configuration.ReceiveTimeout = -1;
        ////redis.Configuration.Password = "aqswdefrgthyjukilo";
        //redis.RenewConnectionPeriod = 20;

        //redis.Connect();  // Needed?
        
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void SendHighScore(int score, string name)
    {
        
        try
        {
            // Locks up with WaitComplete!
            // Is sorted set ok? Only 1 score per name.
            redis.SortedSet["highscore"].Add(score, name);
            redis.Hash["scorehash"].Set(name, ""+score);
            //redis.Close();
            //redis.WaitComplete();
        }
        catch
        {
            Debug.Log("Redis connection fail");
        }
    }

    public void GetHighScoreNames()
    {
        returnNameArray = null;
        returnScoreArray = null;
        var th = new Thread(getHighScores);
        th.IsBackground = true;
        th.Start();
        //Debug.Log("GetHighScoreNames end");

        /*
        Thread.Sleep(1000);
        string[] scores;
        try
        {
            // Locks up with WaitComplete!
            // Is sorted set ok? Only 1 score per name.
            scores = redis.SortedSet["highscore"].Range("-10","-1");
            //redis.WaitComplete();
        }
        catch
        {
            Debug.Log("Redis connection fail");
            return new string[] { "CONNECTION FAIL" };
            // Make sure this is not an allowed player name :D
        }
        return scores;
        */

    }

    public string GetScore(string name)
    {
        if (name.Equals("CONNECTION FAIL"))
        {
            return "";
        }
        string score = "";
        try
        {
            // Locks up with WaitComplete!
            // Is sorted set ok? Only 1 score per name.
            score = redis.Hash["scorehash"].Get(name);
            //redis.WaitComplete();
        }
        catch
        {
            Debug.Log("Redis connection fail");
            return "-1";
        }
        return score;
    }

    private void getHighScores()
    {
        //redis.Connect();
        string[] scores;
        try
        {
            //Debug.Log("Attempting to fetch high score names");
            // Locks up with WaitComplete!
            // Is sorted set ok? Only 1 score per name.
            scores = redis.SortedSet["highscore"].Range("-10", "-1");
            returnNameArray = scores;
            //Debug.Log("Found " + returnNameArray.Length + " names.");
            //redis.WaitComplete();
        }
        catch
        {
            Debug.Log("Redis connection fail");
            returnNameArray = new string[] { "CONNECTION FAIL" };
            // Make sure this is not an allowed player name :D
        }
        if (returnNameArray[0].Equals("CONNECTION FAIL"))
        {
            returnScoreArray = new string[] { "" };
            return;
        }
        else
        {
            string tempScore = "";
            returnScoreArray = new string[returnNameArray.Length];
            for (int i = 0; i < returnNameArray.Length; i++)
            {
                try
                {
                    tempScore = redis.Hash["scorehash"].Get(returnNameArray[i]);
                    Debug.Log("TempScore: " + tempScore );
                }
                catch
                {
                    tempScore = "???";
                }
                returnScoreArray[i] = tempScore;
            }
        }
        //Debug.Log("getHighScores end");
       //Debug.Log("Return array sizes: " + returnScoreArray.Length + ", " + returnNameArray.Length);
        Thread.CurrentThread.Abort();
    }
}
