﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

// Manage the state in one level
// Also manage GUI?
public class Single_LevelManager : MonoBehaviour
{

    // Should these be set here?
    public static int ShellPenetration;
    public static float ShellSpeed;

    public float EnemySpawnSpeed = 5.0f;
    public int EnemiesInLevel = 20;
    public string NextLevelName = "LocalLevel_01";
    private float startTime;

    //public 

    public Text StateText;
    public Text PlayerNameText;
    public Text ScoreText;
    public Text LivesText;
    public Text EnemiesRemainingNum;
    public Text ScoreNumWin;
    public Text ScoreNumLose;
    public GameObject Star1;
    public GameObject Star2;
    public GameObject Star3;
    public GameObject Shield;

    public Text PowerAltText;
    public GameObject FinishGameScreenLose;
    public GameObject FinishGameScreenWin;
    public GameObject PauseSplash;

    public List<AIType> EnemyList;
    // Prefabs
    public GameObject AIPlayerBasic;
    public GameObject AIPlayerFast;
    public GameObject AIPlayerPower;
    public GameObject AIPlayerArmor;

    public int EnemyIndex = 0;
    public Single_WallContainer WallCont;
    public GameObject GearSprite;

    //public List<string> PUNames = new List<string>();
    public List<GameObject> PUPrefabs = new List<GameObject>();

    private LevelState lState = LevelState.Playing;
    private GameObject[] enemySpawnPoints;
    private GameObject[] playerSpawnPoints;
    private GameObject[] puSpawnPoints;

    public Dictionary<int, int> playerPowers;
    public PathFinder PathFinder;
    //public int FindX = 0;
    //public int FindY = 0;
    private bool winCheckRunning = false;
    public GameObject PlayerStatsPrefab;
    private PlayerStats ps;

    public LevelState State
    {
        get { return lState; }
        set { }
    }

    public GameObject[] PlayerSpawnPoints
    {
        get { return playerSpawnPoints; }
        set { }
    }


    void Start()
    {
        // Do level init
        if (ps == null)
        {
            PlayerStats tempps = FindObjectOfType<PlayerStats>();
            if (tempps == null)
            {
                GameObject newps = (GameObject)Instantiate(PlayerStatsPrefab, Vector3.zero, Quaternion.identity);
                ps = newps.GetComponent<PlayerStats>();
            }
            else
            {
                ps = tempps;
            }
        }
        Single_PlayerMovement plr = FindObjectOfType<Single_PlayerMovement>();
        plr.PlayerName = ps.Name;
        plr.PlayerScore = ps.Score;
        plr.Stars = ps.Stars;
        plr.Tanks = ps.Tanks;

        WallCont = FindObjectOfType<Single_WallContainer>();
        StateText.text = stateToString(State);
        FinishGameScreenLose.SetActive(false);
        FinishGameScreenWin.SetActive(false);
        enemySpawnPoints = GameObject.FindGameObjectsWithTag("EnemySpawnPoint");
        playerSpawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint");
        puSpawnPoints = GameObject.FindGameObjectsWithTag("PowerUpSpawnPoint");

        Debug.Log("Enemy spawn points found: " + enemySpawnPoints.Length);
        Debug.Log("Player spawn points found: " + playerSpawnPoints.Length);
        Debug.Log("PowerUp spawn points found: " + puSpawnPoints.Length);
        if (playerPowers == null)
        {
            playerPowers = new Dictionary<int, int>();
        }
        Time.timeScale = 1.0f;
        StartCoroutine(UpdatePlayerStats());
        startTime = Time.time;
        PauseSplash.SetActive(false);
        EnemiesRemainingNum.text = "" + EnemiesInLevel;
    }

    void Update()
    {
        // Debug keys
        if (State == LevelState.Playing)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                //PowerUpSpawnChance();
            }
            if (Input.GetKeyDown(KeyCode.Y))
            {
                //FindObjectOfType<WallContainer>().RestoreBaseWalls();
            }
            //if (Input.GetKeyDown(KeyCode.K))
            //{
            //    List<Vec2i> path = PathFinder.FindPath(0, 0, FindX, FindY);
            //    Debug.Log("Path length: " + path.Count);
            //    //foreach (Vec2i v in path)
            //    //{
            //    //    Debug.Log("[" + v.x + ", " + v.y + "]");
            //    //}
            //}

        }
        if (State == LevelState.Playing || State == LevelState.Paused)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                PauseGame();
            }
        }
        if (State == LevelState.FinishedLose)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                resetLevel();
                //photonView.RPC("resetLevel", PhotonTargets.AllBufferedViaServer);
            }
        }
        if (State == LevelState.FinishedWin)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                // Next level
                //PhotonNetwork.LoadLevel("NetworkGameTest");
                Single_PlayerMovement plr = FindObjectOfType<Single_PlayerMovement>();
                ps.Name = plr.PlayerName;
                ps.Score = plr.PlayerScore;
                ps.Stars = plr.Stars;
                ps.Tanks = plr.Tanks;
                FinishGameScreenWin.SetActive(false);
                SceneManager.LoadScene(NextLevelName);

                //resetLevel();
                //photonView.RPC("resetLevel", PhotonTargets.AllBufferedViaServer);
            }
        }
        if (State == LevelState.FinishedLose || State == LevelState.FinishedWin)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Time.timeScale = 1.0f;
                Application.Quit();
            }
            if (Input.GetKeyDown(KeyCode.M))
            {
                Time.timeScale = 1.0f;
                SceneManager.LoadScene("MainMenu");
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                // Submit score
                Single_PlayerMovement tmpPlr = FindObjectOfType<Single_PlayerMovement>();
                FindObjectOfType<RedisClient>().SendHighScore(tmpPlr.PlayerScore, tmpPlr.PlayerName);
            }
        }

        if (EnemyIndex < EnemiesInLevel)
        {
            if (startTime + EnemySpawnSpeed < Time.time)
            {
                StartCoroutine(spawnEnemy());
            }
        }
        else
        {
            // Check if all enemies are dead
            //Debug.Log("20 enemies spawned");
            // Lol, starts million coroutines
            if (!winCheckRunning)
            {
                winCheckRunning = true;
                StartCoroutine(CheckForLiveEnemies());
            }
        }

    }

    //private IEnumerator submitScore()
    //{

    //}

    private IEnumerator spawnEnemy()
    {
        startTime += EnemySpawnSpeed;
        Vector3 enemyLoc = GetRandomEnemySpawnPoint().transform.position;
        GearSprite.transform.position = new Vector3(enemyLoc.x, enemyLoc.y, -3);
        GearSprite.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        GearSprite.SetActive(false);
        GameObject newEnemy;
        switch (EnemyList[EnemyIndex])
        {
            case AIType.Basic:
            default:
                newEnemy = (GameObject)Instantiate(AIPlayerBasic, enemyLoc, Quaternion.identity);
                newEnemy.GetComponent<Single_AIMovement>().SetEnemyStats(EnemyList[EnemyIndex]);
                break;
            case AIType.Fast:
                newEnemy = (GameObject)Instantiate(AIPlayerFast, enemyLoc, Quaternion.identity);
                newEnemy.GetComponent<Single_AIMovement>().SetEnemyStats(EnemyList[EnemyIndex]);
                break;
            case AIType.Power:
                newEnemy = (GameObject)Instantiate(AIPlayerPower, enemyLoc, Quaternion.identity);
                newEnemy.GetComponent<Single_AIMovement>().SetEnemyStats(EnemyList[EnemyIndex]);
                break;
            case AIType.Armor:
                newEnemy = (GameObject)Instantiate(AIPlayerArmor, enemyLoc, Quaternion.identity);
                newEnemy.GetComponent<Single_AIMovement>().SetEnemyStats(EnemyList[EnemyIndex]);
                break;
        }
        //GameObject newEnemy = (GameObject)Instantiate(AIPlayer, enemyLoc, Quaternion.identity);
        //newEnemy.GetComponent<Single_AIMovement>().SetEnemyStats(EnemyList[EnemyIndex]);
        EnemyIndex++;
        EnemiesRemainingNum.text = "" + (EnemiesInLevel - EnemyIndex);
    }

    public void PowerUpSpawnChance()
    {
        if (Random.Range(1, 10) >= 7)
        {
            Instantiate(PUPrefabs[Random.Range(0, PUPrefabs.Count)], GetRandomPUSpawnPoint().transform.position, Quaternion.identity);
        }
    }

    public IEnumerator UpdatePlayerStats()
    {
        yield return new WaitForSeconds(1.0f);
        if (State == LevelState.Playing)
        {
            PowerAltText.text = "";
            foreach (Single_PlayerMovement p in GameObject.FindObjectsOfType<Single_PlayerMovement>())
            {
                PlayerNameText.text = p.PlayerName;
                ScoreText.text = "" + p.PlayerScore;
                LivesText.text = "Tanks: " + p.Tanks;
                if (p.State == PlayerState.Armored)
                {
                    Shield.SetActive(true);
                }
                else
                {
                    Shield.SetActive(false);
                }
                if (p.ShellPower == ShotPower.High)
                {
                    Star1.SetActive(true);
                    Star2.SetActive(true);
                    Star3.SetActive(true);
                }
                else if (p.ShellPower == ShotPower.Med)
                {
                    Star1.SetActive(false);
                    Star2.SetActive(true);
                    Star3.SetActive(true);
                }
                else
                {
                    Star1.SetActive(false);
                    Star2.SetActive(false);
                    Star3.SetActive(true);
                }
                //PowerAltText.text += p.PlayerName + ":\n" + p.PlayerScore + "\n"
                //+ "Lives: " + p.Tanks + "\n" +
                //"Power: " + (int)p.ShellPower;

            }
        }
        StartCoroutine(UpdatePlayerStats());
    }

    public IEnumerator CheckForLiveEnemies()
    {
        while (FindObjectsOfType<Single_AIMovement>().Length != 0)
        {
            if (EnemyIndex == 0)
            {
                // Game reset
                yield break;
            }
            yield return new WaitForSeconds(1.0f);
        }
        WinGame();
    }

    public void PauseGame()
    {
        if (State == LevelState.Playing)
        {
            lState = LevelState.Paused;
            PauseSplash.SetActive(true);
            StateText.text = stateToString(State);
            Time.timeScale = 0.0f;
        }
        else if (State == LevelState.Paused)
        {
            lState = LevelState.Playing;
            PauseSplash.SetActive(false);
            StateText.text = stateToString(State);
            Time.timeScale = 1.0f;
        }
    }

    public void FinishGame()
    {
        lState = LevelState.FinishedLose;
        StateText.text = stateToString(State);
        FinishGameScreenLose.SetActive(true);
        ScoreNumLose.text = "" + GameObject.FindObjectOfType<Single_PlayerMovement>().PlayerScore;
        StartCoroutine(waitSecThenPause(1.0f));
        // Submit high scores, etc
    }

    public void WinGame()
    {
        lState = LevelState.FinishedWin;
        StateText.text = stateToString(State);
        FinishGameScreenWin.SetActive(true);
        SoundManager snd = FindObjectOfType<SoundManager>();
        snd.SoundSource.PlayOneShot(snd.Victory);
        ScoreNumWin.text = "" + GameObject.FindObjectOfType<Single_PlayerMovement>().PlayerScore;
        Time.timeScale = 0.0f;
    }

    IEnumerator waitSecThenPause(float t)
    {
        yield return new WaitForSeconds(t);
        Time.timeScale = 0.0f;
    }

    private string stateToString(LevelState state)
    {
        switch (state)
        {
            case LevelState.Loading:
                return "Loading";
            case LevelState.Starting:
                return "Starting";
            case LevelState.Playing:
                return "Playing";
            case LevelState.FinishedLose:
                return "Finished";
            case LevelState.Paused:
                return "Paused";
            case LevelState.FinishedWin:
                return "Victory";
            default:
                return "Unknown";
        }
    }
  
    private void resetLevel()
    {
        // Make sure this happens in RPC
        FindObjectOfType<Single_WallContainer>().ResetLevel();
        startTime = Time.time;
        EnemyIndex = 0;
        EnemiesRemainingNum.text = "" + EnemiesInLevel;
        foreach (Single_PlayerMovement plr in FindObjectsOfType<Single_PlayerMovement>())
        {
            // Who does this not always work?
            plr.PlayerReset();
            plr.gameObject.transform.position = GetRandomPlayerSpawnPoint().transform.position;
        }
        foreach (Single_AIMovement ai in FindObjectsOfType<Single_AIMovement>())
        {
            Destroy(ai.gameObject);
        }
        // Destroy powerups?
        lState = LevelState.Playing;

        StateText.text = stateToString(State);
        FinishGameScreenLose.SetActive(false);
        winCheckRunning = false;
        Time.timeScale = 1.0f;
    }

    public GameObject GetRandomEnemySpawnPoint()
    {
        return enemySpawnPoints[Random.Range(0, enemySpawnPoints.Length)];
    }

    public GameObject GetRandomPUSpawnPoint()
    {
        return puSpawnPoints[Random.Range(0, puSpawnPoints.Length)];
    }

    public GameObject GetRandomPlayerSpawnPoint()
    {
        return playerSpawnPoints[Random.Range(0, playerSpawnPoints.Length)];
    }

}
