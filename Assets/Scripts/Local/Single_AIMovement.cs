﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Handle AI
public class Single_AIMovement : MonoBehaviour
{
    // Type of ai? Aggressive, etc
    // Map reset should remove all ai
    public float speed = 20.0f;
    public float ShotCooldownTime = 0.15f;
    public float ShotSpeed = 8f;
    public ShotPower ShellPower = ShotPower.Low;

    public int HitPoints = 1;
    public int WorthPoints = 100;
    public AIType EnemyType;
    public ThreadAnimator ThreadAni;

    public GameObject ShellPrefab;
    public GameObject ShootPoint;
    public AudioSource Audio;
    public AudioClip ShootSound;
    private SoundManager soundMngr;

    public GameObject HitParticles;
    public GameObject DeathParticles;

    public GameObject RayPoint1;
    public GameObject RayPoint2;
    public GameObject RayPoint3;
    public GameObject RayPoint4;

    private PlayerState state;
    private int powerups;

    private bool moving = false;
    public bool ShotCooldown = false;
    private GameObject currentShell;

    private Single_LevelManager levelManager;

    private GameObject targetGO;
    //private Vector3 targetLocation;
    private bool targetSet;
    private List<Vec2i> currentPath;
    private bool PathFindCooldown;

    // Use this for initialization
    void Start()
    {
        levelManager = FindObjectOfType<Single_LevelManager>();
        soundMngr = FindObjectOfType<SoundManager>();
        //targetLocation = FindObjectOfType<BaseScript>().transform.position;
        state = PlayerState.Normal;
        ThreadAni.ThreadsSpinning = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (levelManager.State == LevelState.Playing && state != PlayerState.Frozen)
        {
            if (!moving)
            {
                // Move!
                //randomMove();
                // 
                if (!targetSet)
                {
                    // Sometimes give player as target
                    if (Random.Range(0, 10) > 5)
                    {
                        targetGO = GameObject.FindGameObjectWithTag("Player");
                        //targetLocation = targetGO.transform.position;
                        //Debug.Log("Targeting player");
                    }
                    else
                    {
                        targetGO = GameObject.FindGameObjectWithTag("Base_A");
                        //targetLocation = targetGO.transform.position;
                    }
                    targetSet = true;
                }
                if (currentPath == null || currentPath.Count <= 1 || (targetGO.CompareTag("Base_A") && levelManager.WallCont.MapUpdated || !PathFindCooldown) ||
                    targetGO.CompareTag("Player") && !PathFindCooldown)
                {
                    Vector3 tempPos = transform.position;
                    tempPos.x -= 0.0f;
                    tempPos.x *= 4;
                    tempPos.x += 20 * 2;
                    tempPos.x -= 2;
                    tempPos.y -= 0.0f;
                    tempPos.y *= 4;
                    tempPos.y += 12 * 2;
                    tempPos.y -= 2;
                    Vector3 tempTarget = targetGO.transform.position;
                    tempTarget.x -= 0.0f;
                    tempTarget.x *= 4;
                    tempTarget.x += 20 * 2;
                    tempTarget.x -= 2;
                    tempTarget.y -= 0.0F;
                    tempTarget.y *= 4;
                    tempTarget.y += 12 * 2;
                    tempTarget.y -= 2;
                    //Debug.Log("Calculated player coords: " + tempTarget.x + ", " + tempTarget.y);
                    currentPath = levelManager.PathFinder.FindPath(Mathf.RoundToInt(tempPos.x), Mathf.RoundToInt(tempPos.y), Mathf.RoundToInt(tempTarget.x), Mathf.RoundToInt(tempTarget.y));
                    if (currentPath.Count > 1)
                    {
                        currentPath.RemoveAt(currentPath.Count - 1);
                    }
                    StartCoroutine(PathFindCD(1.5f));
                }
                moveTowardTarget();
            }
            if (!ShotCooldown && currentShell == null)
            {
                // Consider shooting
                int baseWallLayer = LayerMask.NameToLayer("BaseWall");
                Vector3 fwd = transform.TransformDirection(Vector3.up);
                RaycastHit hit;
                bool fire = false;
                if (Physics.Raycast(RayPoint2.transform.position, fwd, out hit, ShotSpeed)) // Make not shoot other enemies
                {
                    if (hit.collider.CompareTag("Player") || hit.collider.CompareTag("RemotePlayer") || hit.collider.CompareTag("Base_A") || hit.collider.gameObject.layer == baseWallLayer)
                    {
                        fire = true;
                    }
                }
                if (Physics.Raycast(RayPoint3.transform.position, fwd, out hit, ShotSpeed))
                {
                    if (hit.collider.CompareTag("Player") || hit.collider.CompareTag("RemotePlayer") || hit.collider.CompareTag("Base_A") || hit.collider.gameObject.layer == baseWallLayer)
                    {
                        fire = true;
                    }
                }
                if (Random.Range(0, 100) > 97)
                {
                    fire = true;

                }
                if (fire)
                {
                    FireShell();
                    StartCoroutine(shotCD(ShotCooldownTime));
                }
            }
        }
    }

    private IEnumerator PathFindCD(float cd)
    {
        PathFindCooldown = true;
        yield return new WaitForSeconds(cd);
        PathFindCooldown = false;
    }

    public void FireShell()
    {
        // With quick movement shells shoot at slightly different time moments
        //GameObject shell = PhotonNetwork.Instantiate("Shell", ShootPoint.transform.position, ShootPoint.transform.rotation, 0);
        currentShell = (GameObject)Instantiate(ShellPrefab, ShootPoint.transform.position, ShootPoint.transform.rotation);
        currentShell.tag = "EnemyShell";
        currentShell.GetComponent<Single_ShellScript>().SetShellStats(Random.Range(5, 5 + (int)ShellPower + 1), ShotSpeed, ShellPower);
        Audio.PlayOneShot(ShootSound);
    }

    private IEnumerator shotCD(float cd)
    {
        ShotCooldown = true;
        yield return new WaitForSeconds(cd);
        ShotCooldown = false;
    }

    private void randomMove()
    {
        Direction randomDir = (Direction)Random.Range(1, 5);
        StartCoroutine(moveStep(randomDir));
    }
    #region Movement
    private void moveTowardTarget()
    {
        if (currentPath != null && currentPath.Count > 1)
        {
            Vec2i nextMove = currentPath[currentPath.Count - 1];
            Vector3 tempPos = transform.position;
            tempPos.x -= 0.0f;
            tempPos.x *= 4;
            tempPos.x += 20 * 2;
            tempPos.x -= 2;
            tempPos.y -= 0.0f;
            tempPos.y *= 4;
            tempPos.y += 12 * 2;
            tempPos.y -= 2;
            Vec2i moveVec = new Vec2i(nextMove.x - Mathf.RoundToInt(tempPos.x), nextMove.y - Mathf.RoundToInt(tempPos.y));
            //Debug.Log("Next ai move: " + nextMove.x + ", " + nextMove.y + ". Location: " + tempPos.x + ", " + tempPos.y);
            //Debug.Log("Move vector: " + moveVec.x + ", " + moveVec.y);
            turnToFace(moveVec);
            if (!canMoveForward())
            {
                return; // Maybe wait for a second?
            }
            else
            {
                if (moveVec.x == 0 && moveVec.y == 1)
                {
                    StartCoroutine(moveStep(Direction.Up));
                }
                else if (moveVec.x == 0 && moveVec.y == -1)
                {
                    StartCoroutine(moveStep(Direction.Down));
                }
                else if (moveVec.x == 1 && moveVec.y == 0)
                {
                    StartCoroutine(moveStep(Direction.Right));
                }
                else if (moveVec.x == -1 && moveVec.y == 0)
                {
                    StartCoroutine(moveStep(Direction.Left));
                }
                else
                {
                    Debug.Log("Abnormal move vector: " + moveVec.x + ", " + moveVec.y);
                    targetGO = GameObject.FindGameObjectWithTag("Base_A");
                    currentPath.Clear();
                }
                if (currentPath.Count > 1)
                {
                    currentPath.RemoveAt(currentPath.Count - 1);
                }
            }

            // can we move?
        }
        //Vector3 fwd = transform.TransformDirection(Vector3.up);
        //Vector3 vectorToBase = targetLocation - transform.position;
        //vectorToBase.z = 0.0f;
        ////Debug.Log("Vector to base: " + vectorToBase);
        //// Check if we get closer to base by moving forward
        ////Debug.Log("Horiz dot: " + Vector3.Dot(new Vector3(vectorToBase.x, 0, 0), fwd));
        ////Debug.Log("Verti dot: " + Vector3.Dot(new Vector3(0, vectorToBase.y, 0), fwd));
        //if (!canMoveForward())
        //{
        //    randomMove();
        //    return;
        //}
        //if (Vector3.Dot(new Vector3(vectorToBase.x, 0, 0), fwd) == 0)
        //{
        //    // Up or down
        //    if (fwd.y < 0 && vectorToBase.y < 0)
        //    {
        //        // Above base
        //        //Debug.Log("Above base");
        //        StartCoroutine(moveStep(Direction.Down));
        //    }
        //    else if (fwd.y > 0 && vectorToBase.y > 0)
        //    {
        //        // Below base
        //        //Debug.Log("Below base");
        //        StartCoroutine(moveStep(Direction.Up));
        //    }
        //    else
        //    {
        //        // Facing away from base
        //        randomMove();
        //    }
        //}
        //else if (Vector3.Dot(new Vector3(0, vectorToBase.y, 0), fwd) == 0)
        //{
        //    // Left or right
        //    // Up or down
        //    if (fwd.x < 0 && vectorToBase.x < 0)
        //    {
        //        // Right side
        //        //Debug.Log("Right of base");
        //        StartCoroutine(moveStep(Direction.Left));
        //    }
        //    else if (fwd.x > 0 && vectorToBase.x > 0)
        //    {
        //        // Left side
        //        //Debug.Log("Left of base");
        //        StartCoroutine(moveStep(Direction.Right));
        //    }
        //    else
        //    {
        //        // Facing away from base
        //        randomMove();
        //    }
        //}
        //else
        //{
        //    if (Mathf.Abs(vectorToBase.x) >= Mathf.Abs(vectorToBase.y))
        //    {
        //        // Move horizontally
        //        if (vectorToBase.x < 0)
        //        {
        //            StartCoroutine(moveStep(Direction.Left));
        //        }
        //        else
        //        {
        //            StartCoroutine(moveStep(Direction.Right));
        //        }
        //    }
        //    else
        //    {
        //        // Move vertically
        //        if (vectorToBase.y < 0)
        //        {
        //            StartCoroutine(moveStep(Direction.Down));
        //        }
        //        else
        //        {
        //            StartCoroutine(moveStep(Direction.Up));
        //        }
        //    }
        //}
    }

    private bool canMoveForward()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.up);

        if (Physics.Raycast(RayPoint1.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint2.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint3.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint4.transform.position, fwd, 0.2f))
        {
            //print("There is something in front of the object!");
            return false;
        }
        else return true;
    }

    private IEnumerator moveStep(Direction dir)
    {
        // Turn
        turnToFace(dir);

        if (canMoveForward())
        {
            // Move
            moving = true;
            ThreadAni.ThreadsSpinning = true;
            Vector3 target = transform.position + getMovementStep(dir);
            Vector3 startPos = transform.position;
            float dt = 0.0f;
            while (!V3Equal(target, transform.position))
            {
                dt += Time.deltaTime * speed;
                transform.position = Vector3.Lerp(startPos, target, dt);
                yield return new WaitForEndOfFrame();
            }
            transform.position = target;
            //yield return new WaitForEndOfFrame();
        }
        moving = false;
        ThreadAni.ThreadsSpinning = false;
        yield return new WaitForSeconds(0.001F);
    }

    private void turnToFace(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
            default:
                transform.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case Direction.Down:
                transform.rotation = Quaternion.Euler(0, 0, 180);
                break;
            case Direction.Left:
                transform.rotation = Quaternion.Euler(0, 0, 90);
                break;
            case Direction.Right:
                transform.rotation = Quaternion.Euler(0, 0, 270);
                break;
        }
    }
    private void turnToFace(Vec2i dir)
    {
        if (dir.x == 0 && dir.y == 1)
        {
            turnToFace(Direction.Up);
        }
        else if (dir.x == 0 && dir.y == -1)
        {
            turnToFace(Direction.Down);
        }
        else if (dir.x == 1 && dir.y == 0)
        {
            turnToFace(Direction.Right);
        }
        else if (dir.x == -1 && dir.y == 0)
        {
            turnToFace(Direction.Left);
        }

    }

    private Vector3 getMovementStep(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return new Vector3(0, 0.25f, 0);
            case Direction.Down:
                return new Vector3(0, -0.25f, 0);
            case Direction.Right:
                return new Vector3(0.25f, 0, 0);
            case Direction.Left:
                return new Vector3(-0.25f, 0, 0);
            default:
                return Vector3.zero;
        }
    }
    #endregion
    public void TakeHit(ShotPower power)
    {
        HitPoints -= (int)power + 1;
        if (HitPoints <= 0)
        {
            state = PlayerState.Dead;
            gameObject.SetActive(false);
            levelManager.PowerUpSpawnChance();
            soundMngr.SoundSource.PlayOneShot(soundMngr.DeathSound);
            if (DeathParticles != null)
            {
                GameObject particles = (GameObject)Instantiate(DeathParticles, transform.position, Quaternion.identity);
                Destroy(particles, 2f);
            }
            Destroy(gameObject, 1f);
        }
        else
        {
            soundMngr.SoundSource.PlayOneShot(soundMngr.HitSoundEnemy);
            GameObject p = (GameObject)Instantiate(HitParticles, transform.position, Quaternion.identity);
            Destroy(p, 2f);
        }
    }

    public void Freeze()
    {
        StartCoroutine(freezeTime(5.0f));
    }

    private IEnumerator freezeTime(float seconds)
    {
        state = PlayerState.Frozen;
        yield return new WaitForSeconds(seconds);
        state = PlayerState.Normal;
    }

    public void SetEnemyStats(AIType type)
    {
        switch (type)
        {
            case AIType.Basic:
            default:
                HitPoints = 1;
                WorthPoints = 100;
                this.speed = 10f;
                ShotSpeed = 6f;
                ShotCooldownTime = 3f;
                ShellPower = ShotPower.Low;
                EnemyType = AIType.Basic;
                break;
            case AIType.Fast:
                HitPoints = 1;
                WorthPoints = 200;
                this.speed = 25f;
                ShotSpeed = 8f;
                ShotCooldownTime = 1.0f;
                ShellPower = ShotPower.Low;
                EnemyType = AIType.Fast;
                //gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.green;
                break;
            case AIType.Power:
                HitPoints = 2;
                WorthPoints = 300;
                this.speed = 15f;
                ShotSpeed = 10f;
                ShotCooldownTime = 1.5f;
                ShellPower = ShotPower.High;
                EnemyType = AIType.Power;
                //gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.yellow;
                break;
            case AIType.Armor:
                HitPoints = 5;
                WorthPoints = 400;
                this.speed = 12f;
                ShotSpeed = 8f;
                ShotCooldownTime = 2.0f;
                ShellPower = ShotPower.Med;
                EnemyType = AIType.Armor;
                //gameObject.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
                break;
        }
    }

    public void SetEnemyStats(int hp, int points, float speed, float shotSpeed, float shotCooldown)
    {
        HitPoints = hp;
        WorthPoints = points;
        this.speed = speed;
        ShotSpeed = shotSpeed;
        ShotCooldownTime = shotCooldown;
        EnemyType = AIType.Custom;
    }

    // Really move this to somewhere else and make it static
    public bool V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.0001;
    }
}
