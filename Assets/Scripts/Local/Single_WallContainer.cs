﻿using UnityEngine;
using System.Collections.Generic;

// Container for all the wall objects in the scene
// Does all the RPC calls for wall destruction
public class Single_WallContainer : MonoBehaviour
{

    public GameObject DestructionParticlesBrick;
    public GameObject DestructionParticlesSteel;
    public GameObject AdamantiumSparkParticles;
    public SoundManager Sound;

    private List<WallScript> levelWalls = null;
    private List<WallScript> baseWalls = null;
    //private GameObject[] waterSquares;

    private byte[,] byteMapWalls;

    public byte[,] ByteMapWalls
    {
        get { return byteMapWalls; }
        set { }
    }

    public bool MapUpdated = true;

    // Use this for initialization
    void Start()
    {
        int baseWallLayer = LayerMask.NameToLayer("BaseWall");
        levelWalls = new List<WallScript>();
        baseWalls = new List<WallScript>();
        //waterSquares = GameObject.FindGameObjectsWithTag("Water");

        byteMapWalls = new byte[20*4, 12*4];

        for (int i = 0; i < 12 * 4; i++)
        {
            for (int j = 0; j < 14 * 4; j++)
            {
                byteMapWalls[j, i] = 0;
            }
        }

        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Water"))
        {
            Vector3 tempPos = go.transform.position;
            tempPos.x += 0.125f;
            tempPos.x *= 4;
            tempPos.x += 20 * 2;
            tempPos.x -= 1;
            tempPos.y += 0.125f;
            tempPos.y *= 4;
            tempPos.y += 12 * 2;
            tempPos.y -= 1;
            int tempX = Mathf.RoundToInt(tempPos.x);
            int tempY = Mathf.RoundToInt(tempPos.y);
            for (int i = -2; i < 2; i++)
            {
                for (int j = -2; j < 2; j++)
                {
                    byteMapWalls[tempX + i, tempY + j] = 1;
                }
            }
        }
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("WallAdamantium")) // Have different loop for small adamantium walls
        {
            Vector3 tempPos = go.transform.position;
            tempPos.x += 0.125f;
            tempPos.x *= 4;
            tempPos.x += 20 * 2;
            tempPos.x -= 1;
            tempPos.y += 0.125f;
            tempPos.y *= 4;
            tempPos.y += 12 * 2;
            tempPos.y -= 1;
            int tempX = Mathf.RoundToInt(tempPos.x);
            int tempY = Mathf.RoundToInt(tempPos.y);
            for (int i = -2; i < 2; i++)
            {
                for (int j = -2; j < 2; j++)
                {
                    byteMapWalls[tempX + i, tempY + j] = 1;
                }
            }
        }


        //int counter = 0;
        foreach (WallScript w in FindObjectsOfType<WallScript>())
        {
            levelWalls.Add(w);
            //if (counter < 1)
            //{
            //    Debug.Log(w.transform.position);
            //    w.GetComponent<MeshRenderer>().material.color = Color.red;
            //    counter++;
            //}
            if (w.gameObject.layer != baseWallLayer) // Add base walls with different number?
            {
                Vector3 tempPos = w.transform.position;
                tempPos.x += 0.125f;
                tempPos.x *= 4;
                tempPos.x += 20 * 2;
                tempPos.x -= 1;
                tempPos.y += 0.125f;
                tempPos.y *= 4;
                tempPos.y += 12 * 2;
                tempPos.y -= 1;
                
                byteMapWalls[Mathf.RoundToInt(tempPos.x), Mathf.RoundToInt(tempPos.y)] = 1;
            }
        }
        Debug.Log("Found " + levelWalls.Count + " wall segments");
        foreach (WallScript w in levelWalls)
        {
            if (w.gameObject.layer == baseWallLayer)
            {
                baseWalls.Add(w);
            }
        }
        Debug.Log("Found " + baseWalls.Count + " basewall segments");

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void RestoreBaseWalls()
    {
        foreach (WallScript w in baseWalls)
        {
            w.gameObject.SetActive(true);
        }
    }

    public void DestroyWall(WallScript wall)
    {
        Vector3 tempPos = wall.transform.position;
        tempPos.x += 0.125f;
        tempPos.x *= 4;
        tempPos.x += 20 * 2;
        tempPos.x -= 1;
        tempPos.y += 0.125f;
        tempPos.y *= 4;
        tempPos.y += 12 * 2;
        tempPos.y -= 1;
        byteMapWalls[Mathf.RoundToInt(tempPos.x), Mathf.RoundToInt(tempPos.y)] = 0;
        MapUpdated = true;

        int wallIndex = levelWalls.IndexOf(wall);
        levelWalls[wallIndex].gameObject.SetActive(false);
        if (levelWalls[wallIndex].gameObject.CompareTag("WallBrick"))
        {
            GameObject particle = (GameObject)Instantiate(DestructionParticlesBrick, levelWalls[wallIndex].transform.position, Quaternion.identity);
            Destroy(particle, 3f);
        }
        else
        {
            GameObject particle = (GameObject)Instantiate(DestructionParticlesSteel, levelWalls[wallIndex].transform.position, Quaternion.identity);
            Destroy(particle, 3f);
        }
        Sound.PlayExplosion();
    }

    public void SpawnAdamantiumSparks(Vector3 position)
    {
        GameObject particle = (GameObject)Instantiate(AdamantiumSparkParticles, position, Quaternion.identity);
        //Sound.PlayExplosion();
        Destroy(particle, 1.5f);
    }

    public void ResetLevel()
    {
        foreach (Single_ShellScript shell in FindObjectsOfType<Single_ShellScript>())
        {
            Destroy(shell.gameObject);  // Make sure no old shells fly around
        }
        int baseWallLayer = LayerMask.NameToLayer("BaseWall");
        foreach (WallScript wall in levelWalls)
        {
            wall.gameObject.SetActive(true);
            if (wall.gameObject.layer != baseWallLayer) // Add base walls with different number?
            {
                Vector3 tempPos = wall.transform.position;
                tempPos.x += 0.125f;
                tempPos.x *= 4;
                tempPos.x += 20 * 2;
                tempPos.x -= 1;
                tempPos.y += 0.125f;
                tempPos.y *= 4;
                tempPos.y += 12 * 2;
                tempPos.y -= 1;

                byteMapWalls[Mathf.RoundToInt(tempPos.x), Mathf.RoundToInt(tempPos.y)] = 1;
            }
        }
    }
}
