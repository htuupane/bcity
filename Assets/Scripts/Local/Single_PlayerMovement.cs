﻿using UnityEngine;
using System.Collections;

public class Single_PlayerMovement : MonoBehaviour
{

    public float speed = 20.0f;
    public float ShotCooldownTime = 0.15f;
    public ShotPower ShellPower = ShotPower.Low;
    public int Tanks = 2;
    public int PlayerScore = 0;
    public string PlayerName = "Player";
    public GameObject TankModel;
    public GameObject ShellPrefab;
    public GameObject ShootPoint;
    public GameObject SparkParticles;
    public GameObject HitParticles;
    public GameObject DeathParticles;
    public GameObject InvSphere;

    public AudioSource Audio;
    private SoundManager soundMngr;
    //public Collider FrontCollider;
    public GameObject RayPoint1;
    public GameObject RayPoint2;
    public GameObject RayPoint3;
    public GameObject RayPoint4;
    public BoxCollider HitBox;

    public Animation CameraShake;
    
    private PlayerState state;
    private int stars;

    private bool moving = false;
    public bool ShotCooldown = false;
    private GameObject currentShell;

    private Rigidbody rb;
    private Single_LevelManager levelManager;

    public PlayerState State
    {
        get { return state; }
        set { }
    }

    public int Stars
    {
        get { return stars; }
        set { SetStars(value); }
    }

    // Use this for initialization
    void Start()
    {
        InvSphere.SetActive(false);
        stars = 0;
        levelManager = FindObjectOfType<Single_LevelManager>();
        soundMngr = FindObjectOfType<SoundManager>();
        rb = GetComponent<Rigidbody>();
        state = PlayerState.Normal;
        //levelManager.SetPlayerPower(0, powerups); // Give name
    }

    // Update is called once per frame
    void Update()
    {
        // Debug
        //if (Input.GetKeyDown(KeyCode.B))
        //{
        //    CameraShake.Play();
        //}
        if (levelManager.State == LevelState.Playing)
        {
            if (state == PlayerState.Dead)
            {
                return; // Need keypresses?
            }
            if (!moving && state != PlayerState.Immobile)
            {
                if (Input.GetButton("up"))          // +Y
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    StartCoroutine(moveStep(Direction.Up));
                    //transform.Translate(Vector3.up * speed * Time.deltaTime, Space.World);
                }
                else if (Input.GetButton("down"))   // -Y
                {
                    transform.rotation = Quaternion.Euler(0, 0, 180);
                    StartCoroutine(moveStep(Direction.Down));
                    //transform.Translate(Vector3.down * speed * Time.deltaTime, Space.World);
                }
                else if (Input.GetButton("left"))   // -X
                {
                    transform.rotation = Quaternion.Euler(0, 0, 90);
                    StartCoroutine(moveStep(Direction.Left));
                    //transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
                }
                else if (Input.GetButton("right"))  // +X
                {
                    transform.rotation = Quaternion.Euler(0, 0, 270);
                    StartCoroutine(moveStep(Direction.Right));
                    //transform.Translate(Vector3.right * speed * Time.deltaTime, Space.World);
                }
            }
            if (Input.GetButtonDown("fire"))
            {
                // Shoot
                if (!ShotCooldown && currentShell == null)
                {
                    //Instantiate(ShellPrefab, ShootPoint.transform.position, ShootPoint.transform.rotation);
                    //Gameobject clone = (Gameobject)PhotonNetwork.Instantiate("Shell", transform.position, transform.rotation);
                    //GameObject shell = PhotonNetwork.Instantiate("Shell", ShootPoint.transform.position, ShootPoint.transform.rotation, 0);
                    //object[] prms = new object[3];
                    //prms[0] = (object)ShootPoint.transform.position;
                    //prms[1] = (object)ShootPoint.transform.rotation;
                    //prms[2] = (object)ShellPower;
                    
                    //photonView.RPC("FireShell", PhotonTargets.All, prms);
                    //StartCoroutine(shotCD(ShotCooldownTime));
                    Audio.Play();
                    FireShell();
                }
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                //photonView.RPC("SpawnAI", PhotonTargets.All);
                // Some randomness on targets
                //PhotonNetwork.InstantiateSceneObject("AIPlayer", levelManager.GetRandomEnemySpawnPoint().transform.position, Quaternion.identity, 0, null);
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                //photonView.RPC("PowerUpPickup", PhotonTargets.All, PowerUp.Bomb);
            }
        }
    }

    public void PlayerReset()
    {
        Tanks = 2;
        stars = 0;
        ShellPower = ShotPower.Low;
        state = PlayerState.Normal;
        TankModel.SetActive(true);
    }
    
    public void FireShell()
    {
        currentShell = (GameObject)Instantiate(ShellPrefab, ShootPoint.transform.position, ShootPoint.transform.rotation);
        currentShell.GetComponent<Single_ShellScript>().SetShellStats(5, 8f, ShellPower);
        currentShell.GetComponent<Single_ShellScript>().Shooter = this;
        currentShell.tag = "Shell";
    }

    private bool canMoveForward()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.up);

        if (Physics.Raycast(RayPoint1.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint2.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint3.transform.position, fwd, 0.2f) ||
            Physics.Raycast(RayPoint4.transform.position, fwd, 0.2f))
        {
            return false;
        }
        else return true;
    }

    private IEnumerator moveStep(Direction dir)
    {
        //Debug.Log("Moving. Blocked: " + frontBlocked);
        if (canMoveForward())
        {
            // Move
            moving = true;
            Vector3 target = transform.position + getMovementStep(dir);
            Vector3 startPos = transform.position;
            float dt = 0.0f;
            while (!V3Equal(target, transform.position))
            {
                dt += Time.deltaTime * speed;
                transform.position = Vector3.Lerp(startPos, target, dt);
                yield return new WaitForEndOfFrame();
            }
            transform.position = target;
            //yield return new WaitForEndOfFrame();
        }
        moving = false;
        yield return new WaitForSeconds(0.001F);
    }

    private IEnumerator shotCD(float cd)
    {
        ShotCooldown = true;
        yield return new WaitForSeconds(cd);
        ShotCooldown = false;
    }

    private Vector3 getMovementStep(Direction dir)
    {
        switch (dir)
        {
            case Direction.Up:
                return new Vector3(0, 0.25f, 0);
            case Direction.Down:
                return new Vector3(0, -0.25f, 0);
            case Direction.Right:
                return new Vector3(0.25f, 0, 0);
            case Direction.Left:
                return new Vector3(-0.25f, 0, 0);
            default:
                return Vector3.zero;
        }
    }

    // Is this the best place to do this?
    public void PowerUpPickup(PowerUp p)
    {
        switch (p)
        {
            case PowerUp.Star:
                stars++;
                //levelManager.SetPlayerPower(0, powerups); // Name
                if (stars > 2 && stars < 6)
                {
                    ShellPower = ShotPower.Med;
                }
                else if (stars >= 6)
                {
                    ShellPower = ShotPower.High;
                }
                soundMngr.SoundSource.PlayOneShot(soundMngr.PowerUpSound);
                break;
            case PowerUp.Bomb:
                foreach (GameObject g in GameObject.FindGameObjectsWithTag("AIPlayer"))
                {
                    GameObject prt = (GameObject)Instantiate(HitParticles, g.transform.position, Quaternion.identity);
                    Destroy(prt, 2f);
                    Destroy(g); // Explosion? AI should have "die" method
                }
                soundMngr.SoundSource.PlayOneShot(soundMngr.ExplosionBomb);
                CameraShake.Play();
                break;
            case PowerUp.Shield:
                state = PlayerState.Armored;
                //Debug.Log("Shield pickup, name: " + PlayerName);
                soundMngr.SoundSource.PlayOneShot(soundMngr.PowerUpSound);
                break;
            case PowerUp.Invincibility:
                StartCoroutine(invincibility(3.0f));
                soundMngr.SoundSource.PlayOneShot(soundMngr.InvincibilityPickup);
                break;
            case PowerUp.Shovel:
                // Repair base walls
                FindObjectOfType<Single_WallContainer>().RestoreBaseWalls();
                soundMngr.SoundSource.PlayOneShot(soundMngr.PowerUpSound);
                break;
            case PowerUp.ExtraLife:
                Tanks++;
                soundMngr.SoundSource.PlayOneShot(soundMngr.PowerUpSound);
                break;
            case PowerUp.StopWatch:
                // Freeze enemies
                foreach (GameObject g in GameObject.FindGameObjectsWithTag("AIPlayer"))
                {
                    g.GetComponent<Single_AIMovement>().Freeze();
                }
                soundMngr.SoundSource.PlayOneShot(soundMngr.PowerUpSound);
                break;
            default:
                Debug.Log("Attempting to use unknown powerup");
                break;
        }
        //soundMngr.SoundSource.PlayOneShot(soundMngr.PowerUpSound);
    }

    public void SetStars(int n)
    {
        if (n < 3)
        {
            ShellPower = ShotPower.Low;
        }
        else if (n < 6)
        {
            ShellPower = ShotPower.Med;
        }
        else
        {
            ShellPower = ShotPower.High;
        }
    }
    
    private IEnumerator invincibility(float time)
    {
        PlayerState originalState = state;
        if (state != PlayerState.Normal || state != PlayerState.Armored)
        {
            originalState = PlayerState.Normal;
        }
        InvSphere.SetActive(true);
        state = PlayerState.Invincible;
        //MeshRenderer tankMesh = TankModel.GetComponentInChildren<MeshRenderer>();
        //Color origCol = tankMesh.material.color;
        //tankMesh.material.color = Color.red;
        yield return new WaitForSeconds(time);
        //tankMesh.material.color = origCol;
        InvSphere.SetActive(false);
        state = originalState;
    }
    
    public void EnemyHit()
    {
        // Die unless invincible or armored
        if (state == PlayerState.Invincible)
        {
            return;
        }
        else if (state == PlayerState.Armored)
        {
            soundMngr.SoundSource.PlayOneShot(soundMngr.HitSound); // Other sound?
            GameObject p = (GameObject)Instantiate(HitParticles, transform.position, Quaternion.identity);
            Destroy(p, 2f);
            state = PlayerState.Normal;
        }
        else
        {
            // Die
            Tanks--;
            soundMngr.SoundSource.PlayOneShot(soundMngr.HitSound);
            if (Tanks <= 0)
            {
                // Game over
                //Debug.Log("Player dead!");
                state = PlayerState.Dead;
                TankModel.SetActive(false);
                transform.position = new Vector3(100, 100, 10);
                levelManager.FinishGame();
            }
            else
            {
                // Respawn
                StartCoroutine(deathAnimation());
                //Debug.Log("Player respawn");
            }
        }
    }

    private IEnumerator deathAnimation()
    {
        state = PlayerState.Dead;
        HitBox.enabled = false;
        TankModel.SetActive(false);
        GameObject p = (GameObject)Instantiate(DeathParticles, transform.position, Quaternion.identity);
        Destroy(p, 2f);
        //rb.constraints = RigidbodyConstraints.FreezePosition;
        //rb.AddForce(new Vector3(Random.Range(0.5f, 2.5f), Random.Range(0.5f, 2.5f), 1f), ForceMode.Impulse);
        //rb.AddTorque(new Vector3(Random.Range(2.5f, 7.5f), Random.Range(1.5f, 4.5f), Random.Range(0.5f, 8.5f)), ForceMode.Impulse);

        yield return new WaitForSeconds(2f);
        state = PlayerState.Normal;
        //rb.constraints = RigidbodyConstraints.FreezeAll;
        gameObject.transform.position = levelManager.GetRandomPlayerSpawnPoint().transform.position;
        gameObject.transform.rotation = Quaternion.identity;
        // Invicibility for a moment?
        TankModel.SetActive(true);
        HitBox.enabled = true;
        StartCoroutine(invincibility(2.0f));

    }
    
    public void Immobilize()
    {
        if (state != PlayerState.Dead)
        {
            //Debug.Log("Immobilizing player " + photonView.viewID);
            state = PlayerState.Immobile;
            SparkParticles.SetActive(true);
            StartCoroutine(immobileCD(3.0f));
        }
    }

    private IEnumerator immobileCD(float cd)
    {
        state = PlayerState.Immobile;
        yield return new WaitForSeconds(cd);
        state = PlayerState.Normal; // Problems with invincibility?
        SparkParticles.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        // Use punrpc PowerUpPickup
        if (other.CompareTag("PowerUpStar"))
        {
            PowerUpPickup(PowerUp.Star);
        }
        else if (other.CompareTag("PowerUpShield"))
        {
            PowerUpPickup(PowerUp.Shield);
        }
        else if (other.CompareTag("PowerUpInvincibility"))
        {
            PowerUpPickup(PowerUp.Invincibility);
        }
        else if (other.CompareTag("PowerUpExtraLife"))
        {
            PowerUpPickup(PowerUp.ExtraLife);
        }
        else if (other.CompareTag("PowerUpBomb"))
        {
            PowerUpPickup(PowerUp.Bomb);
        }
        else if (other.CompareTag("PowerUpShovel"))
        {
            PowerUpPickup(PowerUp.Shovel);
        }
        else if (other.CompareTag("PowerUpStopWatch"))
        {
            PowerUpPickup(PowerUp.StopWatch);
        }
        Destroy(other.gameObject);
    }

    public bool V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.000001;
    }

}
