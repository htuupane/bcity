﻿using UnityEngine;
using System.Collections;

// Remember who shot shell for multiplayer scoring
public class Single_ShellScript : MonoBehaviour
{
    public float ShellSpeed = 10.0f;
    public int Penetration = 5;

    public Collider ShellCollider;

    public ShotPower power; // What does high do?
    public Single_PlayerMovement Shooter;

    public Transform ShellModel;


    void Start()
    {
        Destroy(gameObject, 10.0f);
    }

    void Update()
    {
        transform.Translate(transform.forward * ShellSpeed * Time.deltaTime, Space.World);
        ShellModel.Rotate(0f, 0f, 200f * Time.deltaTime);

    }

    public void SetShellStats(int pen, float speed, ShotPower power)
    {
        Penetration = pen;
        ShellSpeed = speed;
        this.power = power;
    }
    
    void OnCollisionEnter(Collision collision)
    {
        // "Legacy" wall
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("WallSegment"))
        {
            // Nothing
        }
        else if (collision.gameObject.CompareTag("LevelLimit"))
        {
            Destroy(gameObject, 0.1f);
        }

        else if (collision.gameObject.CompareTag("WallBrick"))
        {
            if (Penetration > 0)
            {
                collision.gameObject.GetComponent<WallScript>().DestroyWall();
                Penetration--;
            }
            if (Penetration <= 0)
            {
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }

        else if (collision.gameObject.CompareTag("WallSteel"))
        {
            if (power >= ShotPower.Med)
            {
                if (Penetration > 0)
                {
                    collision.gameObject.GetComponent<WallScript>().DestroyWall();
                    Penetration--;
                }
                if (Penetration <= 0)
                {
                    gameObject.SetActive(false);
                    Destroy(gameObject);
                }
            }
            else
            {
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }
        else if (collision.gameObject.CompareTag("WallAdamantium"))
        {
            // Some spark particles?
            // Spawns as many as segments are hit
            FindObjectOfType<Single_WallContainer>().SpawnAdamantiumSparks(transform.position);
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("Player"))
        {
            // Player hit
            if (ShellCollider.CompareTag("Shell"))
            {
                // Immobilize player for a moment
                collision.gameObject.GetComponent<Single_PlayerMovement>().Immobilize();
            }
            else if (ShellCollider.CompareTag("EnemyShell"))
            {
                // Kill player
                collision.gameObject.GetComponent<Single_PlayerMovement>().EnemyHit();
            }
            Destroy(gameObject);
        }
        else if (collision.gameObject.CompareTag("AIPlayer"))
        {
            // AI hit
            if (Shooter != null)
            {
                Shooter.GetComponent<Single_PlayerMovement>().PlayerScore += collision.gameObject.GetComponent<Single_AIMovement>().WorthPoints;
                collision.gameObject.GetComponent<Single_AIMovement>().TakeHit(power);
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
            // AI cant hurt itself
        }
        else if (collision.gameObject.CompareTag("Base_A"))
        {
            FindObjectOfType<Single_LevelManager>().FinishGame();
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
    
}