﻿using UnityEngine;

public class ThreadAnimator : MonoBehaviour {

    public MeshRenderer ThreadRenderer;
    public bool ThreadsSpinning = false;
    public float ThreadSpeed = 0.5f;

    private Vector2 currentOffset;

	// Use this for initialization
	void Start () {
        //MeshRenderer rend = GetComponent<MeshRenderer>();
        //rend.material.mainTextureOffset = TextureOffset;
        currentOffset = Vector2.zero;
    }
	
	// Update is called once per frame
	void Update () {
        if (ThreadsSpinning)
        {
            currentOffset.x += Time.deltaTime * ThreadSpeed;  // Tank speed?
            ThreadRenderer.material.mainTextureOffset = currentOffset;
        }
    }
}
